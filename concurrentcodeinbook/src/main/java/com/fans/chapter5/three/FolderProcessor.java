package com.fans.chapter5.three;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * 
 * @author fcs
 * @date 2015-7-24
 * 描述：使用异步的方式在一个文件夹及其子文件夹中来搜索带有指定扩展名的文件。
 * 说明：
 */
public class FolderProcessor extends RecursiveTask<List<String>> {
	private static final long serialVersionUID = -7846273985608315979L;

	private String path;
	private String extension;
	
	public FolderProcessor(String path, String extension) {
		this.path = path;
		this.extension = extension;
	}

	@Override
	protected List<String> compute() {
		//存储文件夹中的名称
		List<String> list = new ArrayList<String>();
		
		//存储子任务，这些子任务将处理文件夹中的子文件夹
		List<FolderProcessor> tasks = new ArrayList<FolderProcessor>();
		
		File file = new File(path);
		
		File content [] = file.listFiles();
		
		if(content != null){
			for (int i = 0; i < content.length; i++) {
				if(content[i].isDirectory()){
					FolderProcessor task = new FolderProcessor(content[i].getAbsolutePath(), extension);
					//如果是子文件夹，使用该方法采用异步的方式执行
					task.fork();
					tasks.add(task);
				}else{
					if(checkFile(content[i].getName())){
						list.add(content[i].getAbsolutePath());
					}
			}
			}
		}
		if(tasks.size()> 50){
			System.out.printf("%S: %d tasks ran.\n",file.getAbsolutePath(),tasks.size());
		}
		addResultsFromTasks(list, tasks);
		return list;
	}
	
	//将通过这个任务而启动的子任务返回的结果增加到文件列表中，
	//参数：字符串列表list, FolderProcessor子任务列表tasks
	private void addResultsFromTasks(List<String> list,List<FolderProcessor> tasks){
		for(FolderProcessor  item: tasks){
			list.addAll(item.join());
		}
	}
	
	public boolean checkFile(String name){
		return name.endsWith(extension);
	}
}
