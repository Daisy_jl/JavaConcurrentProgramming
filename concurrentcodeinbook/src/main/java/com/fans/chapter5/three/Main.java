package com.fans.chapter5.three;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
//
public class Main {
	public static void main(String[] args) {
		ForkJoinPool pool = new ForkJoinPool();
		//FolderProcessor system = new FolderProcessor("C:\\Windows","log");
		//FolderProcessor apps = new FolderProcessor("C:\\Program Files","log");
		FolderProcessor documents = new FolderProcessor("D:\\","log");
		//pool.execute(system);
		//pool.execute(apps);
		pool.execute(documents);
		
		do{
			System.out.println("***********************************");
			System.out.printf("Main: Paramllelism: %d\n",pool.getParallelism());
			System.out.printf("Main: Active Thread: %d\n",pool.getActiveThreadCount());
			System.out.printf("Main: Task Count: %d\n",pool.getQueuedTaskCount());
			System.out.printf("Main: Steal Count: %d\n",pool.getStealCount());
			System.out.println("***********************************");
			
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}while((!documents.isDone()));
		pool.shutdown();
		
		List<String> results ;
		results = documents.join();
		System.out.printf("documents: %d files found.\n",results.size());
		//System.out.printf("Apps: %d files found.\n",results.size());
		//System.out.printf("Documents: %d files found.\n",results.size());

	}
}
