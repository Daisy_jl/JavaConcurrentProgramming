package com.fans.chapter5.two;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;
/**
 * 
 * @author fcs
 * @date 2015-6-17
 * 描述：行任务，将在文档的一部分中查找这个词
 * 说明：
 */
public class LineTask extends RecursiveTask<Integer>   {

	private static final long serialVersionUID = -5169618143429955903L;
	private String line[];
	private int start,end;
	private String word;
	
	public LineTask(String[] line, int start, int end, String word) {
		this.line = line;
		this.start = start;
		this.end = end;
		this.word = word;
	}

	
	//如果end和start属性的差异小于100，将这一组词拆分成两组，然后创建两个
	//新的LineTask对象处理这两组，调用invokeAll()方法在线程池中执行他们。
	@Override
	protected Integer compute() {
		Integer result = null;
		if(end - start < 100){
			result = count(line,start,end, word);
			
		}else{
			int mid = (start + end) / 2;
			LineTask task1 = new LineTask(line,start,mid, word);
			LineTask  task2 = new LineTask(line,mid, end ,word);
			//调用该方法在线程池中执行该方法
			invokeAll(task1,task2);
			try {
				//调用该方法将两个任务返回的值相加，最后返回任务计算的结果
				result = groupResults(task1.get(), task2.get());
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		
		}
		return result;
	}
	
	//查找每一行的单词个数
	private Integer count(String [] line,int start,int end ,String word){
		int counter = 0;
		for(int  i =start;i < end; i++){
			if(line[i].equals(word)){
				counter++;
			}
		}
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return counter;
	}
	
	//计算两个数字的和，并返回结果
	private Integer groupResults(Integer number1,Integer number2){
		Integer result; 
		result = number1+number2;
		return result;
	}
}
