package com.fans.chapter5.two;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;

/**
 * 
 * @author fcs
 * @date 2015-6-17
 * 描述：文档任务，将遍历文档中的每一行来查找这个词
 * 说明：
 */
public class DocumentTask extends RecursiveTask<Integer>{
	private static final long serialVersionUID = -3309913898436499241L;
	
	private String document  [][];
	private int start ,end;
	private String word;

	public DocumentTask(String[][] document, int start, int end, String word) {
		this.document = document;
		this.start = start;
		this.end = end;
		this.word = word;
	}

	/**
	 * 任务分解
	 */
	@Override
	protected Integer compute() {
		Integer  result = null;
		if(end - start< 10){
			result = processLine(document, start, end, word);
		} else{
			int mid = (start + end) / 2;
			DocumentTask task1 = new DocumentTask(document,start,mid,word);
			DocumentTask task2 = new DocumentTask(document,mid,end,word);
			invokeAll(task1,task2);
			try {
				result = groupResults(task1.get(),task2.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	/**
	 * 
	 * 作者：fcs
	 * 说明：查找字符串
	 * 返回：该文档有多少单词
	 * 参数：
	 * 时间：2015-7-24
	 */
	private Integer processLine(String [][] document,int start,int end,String word){
		List<LineTask>  tasks = new ArrayList<LineTask>();
		for(int  i=start;i<end;i++){
			LineTask  task = new LineTask(document[i],0,document[i].length,word);
			tasks.add(task);
		}
		invokeAll(tasks);
		int result = 0;
		//合计这些任务的返回值，并返回结果
		for(int  i =0;i<tasks.size();i++){
			LineTask task = tasks.get(i);
			try {
				result = result+task.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		return new Integer(result);
	}
	
	//计算两个数字的和，并返回结果
	private Integer groupResults(Integer number1,Integer number2){
		Integer result; 
		result = number1+number2;
		return result;
	}

}
