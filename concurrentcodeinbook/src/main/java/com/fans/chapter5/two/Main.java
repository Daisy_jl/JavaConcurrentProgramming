package com.fans.chapter5.two;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		Document doc = new Document();
		String  [][] document = doc.generateDocument(100,100, "the");
		DocumentTask task = new DocumentTask(document,0,100,"the");
		ForkJoinPool  pool = new ForkJoinPool();
		pool.execute(task);
		
		do{
			System.out.println("*************************************");
			System.out.printf("Main: Parallelism : %d\n",pool.getParallelism());
			System.out.printf("Main: Active Threads：%d\n",pool.getActiveThreadCount());
			System.out.printf("Main: Task Count: %d\n",pool.getQueuedTaskCount());
			System.out.printf("Main: Steal Count: %d\n",pool.getStealCount());
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}while(!task.isDone());
		pool.shutdown();
		
		try {
			//调用awaitTermination等待任务执行结束
			pool.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			System.out.printf("Main: The word appears %d in the document",task.get());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}
}
