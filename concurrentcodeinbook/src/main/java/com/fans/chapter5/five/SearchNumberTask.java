package com.fans.chapter5.five;

import java.util.concurrent.RecursiveTask;
import java.util.concurrent.TimeUnit;

public class SearchNumberTask extends RecursiveTask<Integer>{

	private static final long serialVersionUID = 732444005139123852L;
	private int numbers [];
	private int start,end;
	private int number;
	private TaskManager  taskmanager;

	private final static int NOT_FOUND = -1;
	
	public SearchNumberTask(int[] numbers, int start, int end, int number,
			TaskManager taskmanager) {
		this.numbers = numbers;
		this.start = start;
		this.end = end;
		this.number = number;
		this.taskmanager = taskmanager;
	}

	@Override
	protected Integer compute() {
		System.out.println("Task : "+start+" : "+end);
		int ret;
		if(end - start>10){
			ret = launchTasks();
			
		}else{
			ret = lookForNumber();
		}
		return ret;
	}
	
	//将任务分解
	public int launchTasks(){
		int mid = (start +end) / 2;
		SearchNumberTask task = new SearchNumberTask(numbers, start, mid, number, taskmanager);
		SearchNumberTask task2 = new SearchNumberTask(numbers, mid, end, number, taskmanager);
		taskmanager.addTask(task);
		taskmanager.addTask(task2);
		task.fork();
		task2.fork();
		
		int returnValue;
		returnValue = task.join();
		if(returnValue != -1){
			return returnValue;
		}
		returnValue = task2.join();
		return returnValue;
		
	}
	
	//实现查找某个数的方法
	public int lookForNumber(){
		for(int i = start;i< end;i++){
			if(numbers[i] == number){
				System.out.printf("Task: number %d found in position %d\n",number,i);
				taskmanager.cancelTasks(this);
				return i;
			}
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return NOT_FOUND;
	}
	
	
	public void writeCancelMessage(){
		System.out.printf("Task: Cancelled task from %d to %d",start,end);
		
	}
}
