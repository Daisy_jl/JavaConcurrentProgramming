package com.fans.chapter5.five;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		ArrayGenerator  generator = new ArrayGenerator();
		int array [] = generator.generatorArray(100);
		TaskManager manager = new TaskManager();
		ForkJoinPool pool  = new ForkJoinPool();
		SearchNumberTask task = new SearchNumberTask(array, 0, 100, 5, manager);
		//采用该方法异步执行线程池中的任务
		pool.execute(task);
		pool.shutdown();
		
		//等待任务执行结束
		try {
			pool.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main: The program has finished \n");
	}
}
