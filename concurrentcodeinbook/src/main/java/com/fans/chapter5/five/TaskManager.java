package com.fans.chapter5.five;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;

public class TaskManager {
	private List<ForkJoinTask<Integer>> tasks;

	public TaskManager() {
		tasks = new ArrayList<ForkJoinTask<Integer>>();
	}
	
	//增加一个ForkJoinTask到任务列表中
	public void addTask(ForkJoinTask<Integer> task){
		tasks.add(task);
	}
	//当前任务找到该数后，取消其他剩余任务
	public void cancelTasks(ForkJoinTask cancelTask){
		for(ForkJoinTask<Integer> task: tasks){
			if(task!=cancelTask){
				task.cancel(true);
				((SearchNumberTask)task).writeCancelMessage();
			}
		}
	}
}
