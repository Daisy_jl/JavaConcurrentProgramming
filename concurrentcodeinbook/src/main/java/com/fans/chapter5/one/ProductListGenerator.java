package com.fans.chapter5.one;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author fcs
 * @date 2015-7-22
 * 描述：生成随机产品列表
 * 说明：
 */
public class ProductListGenerator {
	//生成随机产品列表
	public List<Product> generate(int size){
		List<Product> ret = new ArrayList<Product>();
		for(int i =0;i< size;i++){
			Product product = new Product();
			product.setName("product "+i);
			product.setPrice(10);
			ret.add(product);
		}
		return ret;
	}
	
}
