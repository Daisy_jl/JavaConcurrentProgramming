package com.fans.chapter5.one;
/**
 * 
 * @author fcs
 * @date 2015-7-22
 * 描述：Product类，用来存储产品的名称和价格。
 * 说明：
 */
public class Product {
	private String name;
	private double price;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
