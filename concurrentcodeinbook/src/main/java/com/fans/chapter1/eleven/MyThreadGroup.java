package com.fans.chapter1.eleven;
/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程组中不可控异常的处理
 * 说明：
 */
public class MyThreadGroup extends ThreadGroup{

	public MyThreadGroup(String name) {
		super(name);
	}
	
	/**
	 * 重写uncaughtException(),当线程组中的任何线程对象抛出异常的时候，这个方法会被调用
	 * 打印异常信息和抛出异常的线程代码到控制台，并且中断线程组的其他线程
	 */
	@Override
	public void uncaughtException(Thread t, Throwable e) {
			System.out.printf("The thread %s has thrown an Exception\n",t.getId());
			e.printStackTrace();
			System.out.println("Terminating the rest of the Threads\n");
			interrupt();
	}

}
