package com.fans.chapter1.four;

import java.util.concurrent.TimeUnit;
/**
 * 本例使用java异常来控制中断，使用递归的时候，不管递归调用了多少次，只要线程检测到自己已经被中断了，就会立刻抛出InterruptedException异常，
 * 然后继续执行run方法
 * @author fcs
 * @date 2015-4-10
 * 描述：
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		
		FileSearch  fileSearch = new FileSearch("C:\\Documents and Settings\\teacher\\workspace\\javaThread", "FileSearch.java");
		Thread thread = new Thread(fileSearch);
		thread.start();
		try{
			TimeUnit.SECONDS.sleep(10);   //等待十秒
		}catch(InterruptedException  e){
			e.printStackTrace();
		}
		thread.interrupt();
	}
}
