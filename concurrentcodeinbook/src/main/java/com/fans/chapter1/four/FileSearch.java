package com.fans.chapter1.four;

import java.io.File;

/**
 * 线程中断的控制
 * @author fcs
 * @date 2015-4-10
 * 描述：文件搜索，根据文件名搜索文件，搜索到后则进行中断
 * 说明：
 */
public class FileSearch implements  Runnable {
	private String initPath;
	private String fileName;
	
	public FileSearch(String initPath, String fileName) {
		super();
		this.initPath = initPath;
		this.fileName = fileName;
	}

	public String getInitPath() {
		return initPath;
	}

	public void setInitPath(String initPath) {
		this.initPath = initPath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * 这个方法检查fileName属性是不是一个目录，如果是就调用processDirectory 方法。procesDirectory方法会抛出
	 * InterruptedException异常，因此必须捕获并处理这个异常
	 */
	@Override
	public void run() {
		File file = new File(initPath);
		if(file.isDirectory()){
			try{
				directoryProcess(file);
			}catch (InterruptedException e) {
				System.out.printf("%s : The seach has been interrupted",Thread.currentThread().getName());
			}
		}
	
	}
	/**
	 * 
	 * 作者：fcs
	 * 描述：获得一个文件夹下的所有文件和子文件夹，并进行处理
	 * 对于每个记录这个方法采用递归调用。并用响应的目录名作为传入参数。
	 * 对于每个文件这个方法将调用fileProcess()方法，处理完所有的文件和文件夹后这个方法
	 * 将检查线程是否被中断了，如果是则抛出InterruptedException异常。
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-10
	 */
	private void directoryProcess(File file)throws InterruptedException{
		File list [] = file.listFiles();
		if(list != null ){
			for(int i =0;i< list.length;i++){
				if(list[i].isDirectory()){
					directoryProcess(list[i]);
				}else{
					fileProcess(list[i]);
				}
			}
			if(Thread.interrupted()){
				throw new InterruptedException();
			}
		}
		
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：比较当前的文件名和要查找的文件名。如果文件名匹配就将信息打印到控制台，
	 * 做完比较后线程将检查是不是被中断了，如果是抛出InterruptedException
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-10
	 */
	private void fileProcess(File file)throws InterruptedException{
		if(file.getName().equals(fileName)){
			System.out.printf("%S : %S\n",Thread.currentThread().getName(),file.getAbsolutePath());
		}
		if(Thread.interrupted()){
			throw new InterruptedException();
		}
	}
	
	

}
