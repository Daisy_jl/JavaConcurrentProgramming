package com.fans.chapter1.ten;

import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：创建线程组对象
 * 说明：这里运行结果不同，需要研究
 */
public class Main {
	public static void main(String[] args) {
		ThreadGroup  threadGroup = new ThreadGroup("Searcher");
		Result result = new Result();
		SearchTask searchTask = new SearchTask(result);
		//线程是逐个输出的，所以线程组输出信息的时候，线程组的大小是0？？？
		for(int i =0 ;i< 5;i++){
			Thread thread = new Thread(threadGroup,searchTask);
			thread.start();
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		//获取线程组包含线程的数量
		System.out.printf("Number of Threads %d\n",threadGroup.activeCount());
		System.out.printf("Information about the thread group \n");
		threadGroup.list();  //这里并没有打印？？
		
		
		Thread [] threads = new Thread[threadGroup.activeCount()];
		threadGroup.enumerate(threads);
		for(int i=0;i<threadGroup.activeCount();i++){
			System.out.printf("Thread %s : %s\n",threads[i].getName(),threads[i].getState());
		}
		waitFinish(threadGroup);
		Thread.interrupted();   //该方法将终端这个组的其余线程
	}
	
	private static void waitFinish(ThreadGroup threadGroup){
		while(threadGroup.activeCount() > 9){
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
