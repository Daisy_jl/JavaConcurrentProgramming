package com.fans.chapter1.ten;

import java.util.Date;
import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程的分组
 * 说明：把一个组的线程当成单一的单元，对组内线程对象进行访问并操作他们
 * java提供ThreadGroup 类表示一组线程，线程组可以包含线程对象，也可以包含其他的线程
 * 组对象，是一个树形结构。
 */
public class SearchTask implements Runnable {
	private Result result;
	
	public SearchTask(Result result) {
		this.result = result;
	}

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		System.out.printf("Thread %s: Start\n",name);
		try {
			doTask();
			result.setName(name);
		} catch (InterruptedException e) {
			System.out.printf("Thread %s :interrupted\n",name);
			return ;
		}
		System.out.printf("Thread %s End\n",name);
	}
	private void doTask() throws InterruptedException{
		Random random = new Random((new Date()).getTime());
		int value = (int)(random.nextDouble()*100);
		System.out.printf("Thread %s : %d\n",Thread.currentThread().getName(),value);
	}
}
