package com.fans.chapter1.three;

public class Main {
	public static void main(String[] args) {
		Thread task = new PrimeGenerator();
		task.start();
		try{
			Thread.sleep(5000);   //休眠5秒后进行中断操作
			
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		task.interrupt();  //终止线程
	}
}
