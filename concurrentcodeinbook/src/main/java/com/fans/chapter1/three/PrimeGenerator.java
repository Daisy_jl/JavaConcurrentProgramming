package com.fans.chapter1.three;
/**
 * 线程的中断设置
 * @author fcs
 * @date 2015-4-10
 * 描述：在打印素数的过程中设置中断
 * 说明：
 */
public class PrimeGenerator extends Thread {
	@Override
	public void run(){
		long number = 1L;
		while(true){
			if(isPrime(number)){
				System.out.printf("number %d is Prime\n",number);
			}
			if(isInterrupted()){
				System.out.println("The Prime Generatro has been Interrupted ");
				return;
			}
			number ++;
		}
	}
	
	private boolean isPrime(long number){
		if(number <= 2){
			return false;
		}
		
		for(long i = 2;i< number;i++){
			if((number % i) == 0){
				return false;
			}
		}
		return true;
	}
	
}
