package com.fans.chapter1.nine;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程局部变量的使用
 * 说明：，对于继承了Thread类或者实现了Runnable接口的对象来说这些所有的线程都将共享相同的变量
 * 如果一个实现了Runnable接口的对象在一个线程中改变了一个树形，所有的线程都会被这个改变影响
 *  
 *  输出结果与书上不一致，共享变量会被修改，不安全
 */
public class UnsafeTask implements Runnable{
	private Date startDate;
	@Override
	public void run() {
		startDate = new Date();
		System.out.printf("Starting Thread %s: %s\n",Thread.currentThread().getId(),startDate);
		try {
			TimeUnit.SECONDS.sleep((int)Math.rint(Math.random()*10));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("Thread Finished %s : %s\n",Thread.currentThread().getId(),startDate);
	}
}
