package com.fans.chapter1.nine;

import java.util.concurrent.TimeUnit;

public class Core {
	public static void main(String[] args) {
		UnsafeTask unsafeTask = new UnsafeTask();
		//启动十个线程每个线程启动时间为两秒
		for(int i =0 ;i< 10;i++){
			Thread thread = new Thread(unsafeTask);
			thread.start();
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
