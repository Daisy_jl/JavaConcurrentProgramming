package com.fans.chapter1.nine;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：使用线程局部变量来解决线程共享变量的问题
 * 说明：
 */
public class SafeTasks implements Runnable{
	private static ThreadLocal<Date> startDate = new ThreadLocal<Date>(){
		protected Date initialValue() {
			return new Date();
		};
	};
	
	@Override
	public void run() {
		System.out.printf("Starting Thread :%s : %s\n",Thread.currentThread().getId(),startDate.get());
		try {
			TimeUnit.SECONDS.sleep((int)Math.rint(Math.random()*10));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("Thread Finished : %s : %s\n",Thread.currentThread().getId(),startDate.get());
	}
}
