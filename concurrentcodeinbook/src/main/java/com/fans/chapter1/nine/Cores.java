package com.fans.chapter1.nine;

import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程局部变量的安全方式
 * 说明：
 */
public class Cores {
	public static void main(String[] args) {
		SafeTasks  safeTasks = new SafeTasks();
		for(int i =0 ;i<10;i++){
			Thread thread = new Thread(safeTasks);
			thread.start();
			
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
