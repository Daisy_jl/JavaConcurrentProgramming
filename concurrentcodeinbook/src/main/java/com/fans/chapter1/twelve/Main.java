package com.fans.chapter1.twelve;
/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程工厂的演示
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		MyThreadFactory  threadFactory = new MyThreadFactory("MyThreadFactory");
		Task task = new Task();
		Thread thread;
		System.out.println("String the threads ..");
		for(int i = 0;i< 10;i++){
			thread = threadFactory.newThread(task);
			thread.start();
		}
		System.out.printf("Factory stats:\n");
		System.out.printf("%s\n",threadFactory.getStats());
	}
}
