package com.fans.chapter1.twelve;

import java.util.concurrent.TimeUnit;
/**
 * 线程对象，在线程工厂中批量生产
 */
public class Task implements Runnable{

	@Override
	public void run() {
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
}
