package com.fans.chapter1.twelve;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadFactory;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：使用工厂类创建线程
 * 说明：java提供了ThreadFactory接口，这个接口实现了线程对象工厂
 */
public class MyThreadFactory implements ThreadFactory{

	private int counter;
	private String name;
	private List<String> states;
	
	public MyThreadFactory(String name) {
		counter = 0;
		states = new ArrayList<String>();
		this.name = name;
	}

	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r,name+"-Thread_"+counter);
		states.add(String.format("Created thread %d with name %s on %s\n", t.getId(),t.getName(),new Date()));
		return t;
	}

	/**
	 * 
	 * 作者：fcs
	 * 描述：返回一个字符串对象
	 * 说明：用来表示所有线程对象的统计数据
	 * 返回：
	 * 参数：
	 * 时间：2015-4-10
	 */
	public String getStats(){
		StringBuffer stringBuffer = new StringBuffer();
		Iterator<String> it = states.iterator();
		while(it.hasNext()){
			stringBuffer.append(it.next());
			stringBuffer.append("\n");
		}
		return stringBuffer.toString();
	}
	
	
}
