package com.fans.chapter1.eight;

public class Main {
	public static void main(String[] args) {
		Task task = new Task();
		Thread thread = new Thread(task);
		//设置线程的运行时异常处理器
		thread.setUncaughtExceptionHandler(new ExceptionHandler());
		thread.start();
	}
}
