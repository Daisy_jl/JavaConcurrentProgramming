package com.fans.chapter1.eight;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程中不可控异常的处理
 * 说明：
 */
public class ExceptionHandler  implements  UncaughtExceptionHandler {

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		//运行到这里就结束了，只是输出第一句话
		System.out.printf("An Exception has been captured");
		System.out.printf("Thread %\n",t.getId());
		System.out.printf("Exception: %s\n",e.getClass().getName(),e.getMessage());
		System.out.printf("Stack Trace \n");
		e.printStackTrace(System.out);
		System.out.printf("Thread status: %s \n",t.getState());
		
	}

}
