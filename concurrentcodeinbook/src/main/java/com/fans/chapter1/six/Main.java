package com.fans.chapter1.six;

import java.util.Date;

public class Main {
	public static void main(String[] args) {
		DataSourceLoader  dataSourceLoader = new DataSourceLoader();
		NetWorkConnectionLoader  netWorkConnectionLoader = new NetWorkConnectionLoader();
		Thread thread1 = new Thread(dataSourceLoader);
		Thread thread2 = new Thread(netWorkConnectionLoader);
		thread1.start();
		thread2.start();
		
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("Main: Configration has been loaded: %s \n",new Date());
	
	}
}
