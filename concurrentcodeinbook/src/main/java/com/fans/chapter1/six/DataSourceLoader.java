package com.fans.chapter1.six;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：等待线程的终止
 * 举例：先初始化一些必须的资源，可以使用线程完成一些初始化任务。
 * 等待线程结束，再执行程序的其他任务。
 * 说明：
 */
public class DataSourceLoader implements Runnable{

	@Override
	public void run() {
		System.out.printf("Beginning data source loading: %s\n",new Date());
		try{
			TimeUnit.SECONDS.sleep(4);    //模拟执行四秒
		}catch (Exception e) {
			e.printStackTrace();
		}
		System.out.printf("Data source loadign has finished: %s\n",new Date());
	}

}
