package com.fans.chapter1.five;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程的休眠和恢复
 * 说明：在线程被挂起的时钟周期内，线程什么都不做，不占用计算机资源
 * 当该线程的执行的CPU执行周期来临的时候，JVM会选中它继续执行。
 * 可以使用线程的sleep()方法来达到这个目标。
 * 这里使用sleep方法，每隔一秒就输出实际时间、
 */
public class FileClock implements Runnable {
		@Override
		public void run() {
			for(int i =0;i< 10;i++){
				System.out.printf("%s\n",new Date());
				try{
					TimeUnit.SECONDS.sleep(1);
				}catch (InterruptedException e) {
					e.printStackTrace();
					System.out.println("The fileClock has been interrupted");
				}
			}
		}
}
