package com.fans.chapter1.five;

import java.util.concurrent.TimeUnit;
/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：当线程被中断时，释放或者关闭线程正在使用的资源。
 * 如果休眠中线程被中断，该方法就会立即抛出InterruptedException异常，而不需要等待线程休眠时间结束
 * 说明：
 */
public class FileMain {
	public static void main(String[] args) {
		FileClock  fileClock = new FileClock();
		Thread thread = new Thread(fileClock);
		thread.start();
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread.interrupt();
	}
}
