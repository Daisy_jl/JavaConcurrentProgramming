package com.fans.chapter1.seven;

import java.util.Date;
import java.util.Deque;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：设置该线程为守护线程
 * 说明：
 */
public class CleaerTask extends Thread {
	private int count;   //计数清除的事件数量
	private Deque<Event>  deque;
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public CleaerTask(Deque<Event> deque,int count) {
		this.deque = deque;
		setDaemon(true);
		this.count = count;
	}

	@Override
	public void run() {
		while(true){
			Date date = new Date();
			clean(date);
		}
	}
	/**
	 * 
	 * 作者：fcs
	 * 描述：该方法读取队列的最后一个事件对象，如果这个时间是10秒钟前创建的，
	 * 就将它删除并检查下一个，如果有事件被删除，clean将打印出这个事件的信息，
	 * 也打出队列的长度。
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-10
	 */
	private void clean(Date date){
		long difference;
		boolean delete;
		if(deque.size() == 0){
			return ;
		}
		delete = false;
		do{
			Event e = deque.getLast();
			difference = date.getTime() - e.getDate().getTime();
			if(difference > 10000){
				System.out.printf("Cleaner: %s \n",e.getEvent());
				deque.removeLast();
				count ++;
				delete = true;
			}
			
		}while(difference > 10000);
		if(delete){
			System.out.printf("Cleaer : Size of the queue: %d \n",deque.size());
			System.out.println("coutn = "+count);

		}
	}
}
