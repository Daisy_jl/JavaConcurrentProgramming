package com.fans.chapter1.seven;

import java.util.Date;
import java.util.Deque;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：守护线程的创建和运行
 * 说明：
 */
public class WriteTask implements Runnable{
	private Deque<Event>  deque;
	
	public WriteTask(Deque<Event> deque) {
		this.deque = deque;
	}

	public void run() {
		for (int i = 0; i < 100; i++) {
			Event event = new Event();
			event.setDate(new Date());
			event.setEvent(String.format("The thread %s has generated an event", Thread.currentThread().getId()));
			deque.addFirst(event);
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
