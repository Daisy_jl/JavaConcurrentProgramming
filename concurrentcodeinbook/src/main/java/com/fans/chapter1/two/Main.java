package com.fans.chapter1.two;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.Thread.State;

public class Main {
	public static void main(String[] args) {
		Thread [] thread = new Thread[10];
		//保存线程运行的状态
		State status [] = new State[10];
		for(int i =0 ;i< 10;i++){
			thread[i] = new Thread(new Calculator(i));
			if((i % 2 ==0)){
				thread[i].setPriority(Thread.MAX_PRIORITY);
			}
			else {
				thread[i].setPriority(Thread.MIN_PRIORITY);
			}
			thread[i].setName("Thread"+i);
		}
		
		try{
			FileWriter  file = new FileWriter("log.txt");
			PrintWriter pw =new PrintWriter(file);   //这里的pw没有写入文件，不知道为啥、
			//记录线程的状态	
			for(int i =0 ;i< 10;i++){
					pw.println("Main: status of Thread "+ i+ ":"+thread[i].getState());
					status[i] = thread[i].getState();
				}	
			//开始线程
			for(int i =0 ; i < 10;i ++){
				thread[i].start();
			}
			
			
			boolean finish = false;
			while(!finish){
				for(int i = 0 ;i < 10;i++){
					if(thread[i].getState() != status[i]){
						writeThreadInfo(pw,thread[i],status[i]);
						status[i] = thread[i].getState();
					}
				}
				finish  = true;
				for(int i =0;i<10;i++){
						 finish = finish &&(thread[i].getState() == State.TERMINATED);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	//写下线程的相关信息
	private static void writeThreadInfo(PrintWriter pw,Thread thread,State state){
		pw.printf("Main: Id %d - %s\n",thread.getId(), thread.getName());
		pw.printf("Main : Prority: %d\n",thread.getPriority());
		pw.printf("Main : Old state : %s\n",state);
		pw.printf("Main : new State : %s\n", thread.getState());
		pw.printf("Main ************************************");
	}
}
