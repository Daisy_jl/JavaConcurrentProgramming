package com.fans.chapter1.two;
/**
 * 
 * @author fcs
 * @date 2015-4-10
 * 描述：线程信息的获取和设置
 * 说明：第二种方式实现多线程
 */
public class Calculator extends Thread {
	private int number;
	
	public Calculator(int number) {
		super();
		this.number = number;
	}

	@Override
	public void run(){
		for(int i =0;i<10;i++){
			System.out.printf("%s: %d * %d = %d\n",Thread.currentThread().getName(),number,i,number*i);
		}
	}
}
