package com.fans.chapter6.five;

import java.util.concurrent.ConcurrentSkipListMap;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：使用线程安全可遍历映射
 * 说明：
 */
public class Task  implements Runnable{
	private ConcurrentSkipListMap<String,Contact> map;
	private String id;
	
	public Task(ConcurrentSkipListMap<String, Contact> map, String id) {
		this.map = map;
		this.id = id;
	}

	@Override
	public void run() {
		for(int i = 0;i< 1000;i++){
			Contact contact = new Contact(id,String.valueOf(i+1000));
			//这里如果插入的键值已经存在，就用新插入的值覆盖已有的值
			map.put(id+contact.getPhone(), contact);
		}
	}

}
