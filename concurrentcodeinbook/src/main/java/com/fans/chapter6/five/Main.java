package com.fans.chapter6.five;

import java.util.Map;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class Main {
	public static void main(String[] args) {
		ConcurrentSkipListMap<String,Contact>  map = new ConcurrentSkipListMap<String, Contact>();
		Thread threads []= new Thread[25];
		int counter = 0;
		for(char i = 'A';i<'Z';i++){
			Task task = new Task(map,String.valueOf(i));
			threads[counter] = new Thread(task);
			threads[counter].start();
			counter++;
		}
		
		//使用join()方法等待所有线程执行完成。
		
		for(int i = 0;i< 25;i++){
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.printf("Main: Size of the map: %d\n",map.size());
		Map.Entry<String, Contact> element;   //map.entry是map接口的一个内部接口，它的作用就是包装一个map的节点，这个节点
		//封装了key,value以及别的值（比如hashmap中的哈希码和next指针）
		Contact contact;
		//使用firstEntry()方法获取map中的第一个元素
		element = map.firstEntry();  
		contact = element.getValue();
		System.out.printf("Main: First Entry: %s: %s\n",contact.getName(),contact.getPhone());
		
		//使用lastEntry()方法获取map中的最后一个元素
		element = map.lastEntry();
		contact = element.getValue();
		System.out.printf("Main: Last Entry: %s %s\n",contact.getName(),contact.getPhone());
		
		//使用subMap()取得map的一个子映射，将这些数据输出到控制台。
		System.out.println("Main: Submap from A1996 to B1002 :");
		
		//该方法返回含有映射部分元素的ConcurrentNavigableMap对象。
		ConcurrentNavigableMap<String, Contact> submap = map.subMap("A1996", "B1002");
		do{
			element = submap.pollFirstEntry();
			if(element!=null){
				contact = element.getValue();
				System.out.printf("%s: %s\n",contact.getName(),contact.getPhone());
			}
		}while(element != null);
		
		
		
		
	}
}
