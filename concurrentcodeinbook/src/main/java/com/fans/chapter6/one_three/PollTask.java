package com.fans.chapter6.one_three;

import java.util.List;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：从集合中取出元素
 * 说明：
 */
public class PollTask implements Runnable{

	private List<String> list;
	
	public PollTask(List<String> list) {
		this.list = list;
	}

	public  synchronized String poll(List<String> list,int index){
		return list.remove(index);
	}
	 
	@Override
	public void run() {
		for(int  i =0;i<100;i++){
			poll(list,i);
		}
	}

}
