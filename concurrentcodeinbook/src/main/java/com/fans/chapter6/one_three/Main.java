package com.fans.chapter6.one_three;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：创建100个线程添加元素，再创建100个线程取出元素
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
			AddTask task = new AddTask(list);
		Thread addt  = new Thread(task);
		addt.start();
		try {
			addt.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
			
		System.out.println("Main: Size of the list: "+list.size());

			PollTask task1 = new PollTask(list);
			Thread pollt = new Thread(task1);
			pollt.start();		
			try {
				pollt.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		System.out.printf("Main: Size of the list : %d\n",list.size());
	}
	
	
	
}
