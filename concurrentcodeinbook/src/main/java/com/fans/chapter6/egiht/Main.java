package com.fans.chapter6.egiht;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * 
 * @author fcs
 * @date 2015-6-22
 * 描述：使用原子数组
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		final int THREADS = 100;
		AtomicIntegerArray vector = new AtomicIntegerArray(1000);
		Incrementer incrementer = new Incrementer(vector);
		Decrementer  decrementer = new Decrementer(vector);
		Thread threadIn  [] = new Thread[THREADS];
		Thread  threadDe [] = new Thread[THREADS];
		
		for(int i = 0;i < THREADS;i++){
			threadIn[i] = new Thread(incrementer);
			threadDe[i] = new Thread(decrementer);
			threadIn[i].start();
			threadDe[i].start();
		}
		
		for(int i =0 ;i< 100;i++){
			try {
				threadIn[i].join();
				threadDe[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		//使用get()方法获取原子数组中的各个元素，然后在控制台上输出原子数组中不为0的元素
		for(int i =0 ;i< vector.length();i++){
			if(vector.get(i) != 0){
				System.out.println("Vector["+i+"] : "+vector.get(i));
			}
		}
		System.out.println("Main: End of the example.");
		
	}
}
