package com.fans.chapter6.six;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-6-22
 * 描述：生成并发随机数
 * 
 * 说明：java 7 新引入的ThreadLocalRandom类，是线程本地变量。
 * 每个生成随机数的线程都有一个不同的生成器，但是都在同一个类中被管理，对程序员来讲是透明的。
 * 相比于使用共享的Random对象为所有线程生成随机数，这种机制具有更好的性能。
 */
public class TaskLocalRandom implements Runnable{
	
	//实现类构造器，使用current()方法为当前线程初始化随机数生成器
	public TaskLocalRandom(){
		//该方法是一个静态方法，返回与当前线程相关关联的TaskLocalRandom对象，
		//所以可以使用这个对象生成随机数，如果调用该方法的线程还没有关联随机数对象，就会生成一个新的。
		ThreadLocalRandom.current();
	}
	/**
	 * 在该方法中调用current()获取本地线程关联的随机数生成器，同时也调用了NextInt()方法并以数字10作为参数传入。、
	 * 
	 */
	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		for(int i = 0;i < 10000;i++){
			try {
				TimeUnit.MILLISECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.printf("%s: %d\n",name,ThreadLocalRandom.current().nextInt(100));
		}
	}
	
}
