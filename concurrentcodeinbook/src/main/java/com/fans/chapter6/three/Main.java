package com.fans.chapter6.three;

import java.util.concurrent.PriorityBlockingQueue;

public class Main {
	public static void main(String[] args) {
		PriorityBlockingQueue<Event> queue = new PriorityBlockingQueue<Event>();
		Thread [] threads = new Thread[5];
		for(int i =0 ;i< threads.length;i++){
			Task task = new Task(i,queue);
			threads[i] = new Thread(task);
			threads[i].start();

		}
		
		for(int  i=0;i<threads.length;i++){
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.printf("Main�� Queue Size :%d\n",queue.size());
		for(int  i =0 ;i< threads.length*1000;i++){
			Event event = queue.poll();
			System.out.printf("Thread %s: Priority %d\n",event.getThread(),event.getPriority());
		}
		
		System.out.printf("Main: Queue Size: %d\n",queue.size());
		System.out.println("Main: End of the program");
		
		
	}
}
