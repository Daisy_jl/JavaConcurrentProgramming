package com.fans.chapter6.three;

import java.util.concurrent.PriorityBlockingQueue;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：使用按优先级排序的阻塞式线程安全列表
 * 说明：
 * 所有Event都有优先级属性。带有最高优先级的值的元素是队列中第一个元素。当实现compareTo()
 * 方法时，如果Event本身的优先级高于作为参数的event的优先级值，结果返回-1，如果event本身的优先级值
 * 低于作为参数的even的优先级值，结果返回1，如果两个对象的优先级相同，结果返回0，在返回值为0的情况下，PriorityBlockingQueue类
 * 不保证元素的次序
 */
public class Task implements Runnable {
	
	private int id;
	private PriorityBlockingQueue<Event> queue;
	
	public Task(int id, PriorityBlockingQueue<Event> queue) {
		this.id = id;
		this.queue = queue;
	}

	@Override
	public void run() {
		for(int i =0;i<1000;i++){
			Event event = new Event(id,i);
			queue.add(event);
		}
	}

}
