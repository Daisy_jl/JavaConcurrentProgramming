package com.fans.chapter6.three;

public class Event implements Comparable<Event> {

	private int thread;
	private int priority;
	
	public Event(int thread, int priority) {
		this.thread = thread;
		this.priority = priority;
	}

	
	
	public int getThread() {
		return thread;
	}

	public int getPriority() {
		return priority;
	}

	//这里的比较方法，与大多数comparator.compareTo()方法相反
	@Override
	public int compareTo(Event o) {
		if(this.priority > o.getPriority()){
			return -1;
		}else if(this.priority == o.getPriority()){
			return 0;
		}else{
			return 1;
		}
	}
	
	
}
