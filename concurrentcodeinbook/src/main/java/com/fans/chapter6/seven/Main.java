package com.fans.chapter6.seven;

import java.util.concurrent.atomic.AtomicLong;

public class Main {
	public static void main(String[] args) {
		Acount acount = new Acount();
		acount.setBalence(new AtomicLong(1000));
		
		Company  company = new Company(acount);
		Bank bank = new Bank(acount);
		System.out.printf("Acount: Initial Balence: %d\n",acount.getBalence().longValue());
		
		Thread tcompany = new Thread(company);
		Thread tbank = new Thread(bank);
		
		tcompany.start();
		tbank.start();
		
		try {
			//注意使用join()方法，否则下面的输出可能不是真正的输出结果。
			tcompany.join();
			tbank.join();
			System.out.printf("Acount: Final Balence: %d\n",acount.getBalence().longValue());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}
