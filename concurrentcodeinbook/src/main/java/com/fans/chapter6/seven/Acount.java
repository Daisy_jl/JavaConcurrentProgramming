package com.fans.chapter6.seven;

import java.util.concurrent.atomic.AtomicLong;

/**
 * 
 * @author fcs
 * @date 2015-6-22
 * 描述：使用原子变量
 * 说明：使用原子变量模拟银行存取款模型
 */
public class Acount {
	private AtomicLong  balence;

	public Acount() {
		balence = new AtomicLong();
	}

	public AtomicLong getBalence() {
		return balence;
	}

	public void setBalence(AtomicLong balence) {
		this.balence = balence;
	}
	
	//增加balence存款
	public void addBalence(long amount){
		this.balence.getAndAdd(amount);
	}
	
	//减少balence存款
	public void subBalence(long amount){
		this.balence.getAndAdd(- amount);
	}
	
	
	
	
	
	

}
