package com.fans.chapter6.two;

import java.util.Date;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author fcs
 * @date 2015-6-23
 * 描述：该类使用take()方法从列表中取出字符串，如果列表为空，调用这个方法的线程将阻塞直到列表不为空（即有可用元素）
 * 说明：调用这两个方法的线程可能被阻塞，在阻塞是如果线程被中断，方法会抛出InterruptedException异常。
 * 所以必须捕获和处理异常。
 */
public class Main {
	public static void main(String[] args) {
		LinkedBlockingDeque<String> list = new LinkedBlockingDeque<String>(3);
		Client client = new Client(list);
		Thread thread = new Thread(client);
		thread.start();
		for(int i =0;i<5;i++){
			for(int j =0;j<3;j++){
				try {
					String request = list.take();
					System.out.printf("Main: request: %s at %s. Size: %d\n",request,new Date(),list.size());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				try {
					TimeUnit.MILLISECONDS.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Main : End of the program.");
	}
}
