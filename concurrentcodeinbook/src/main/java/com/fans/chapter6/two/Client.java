package com.fans.chapter6.two;

import java.util.Date;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：使用阻塞式线程安全列表
 * 说明：添加大量的数据到一个列表中
 * 
 * 本类使用put()方法将字符串插入到列表中，如果列表（列表生成时指定了容量）已经满的话
 * 调用该方法的线程将被阻塞直到列表中有了可用的空间。
 * 
 * 
 * 
 * 
 */
public class Client implements Runnable {

	private LinkedBlockingDeque<String> requestList;
	
	public Client(LinkedBlockingDeque<String> requestList) {
		this.requestList = requestList;
	}

	@Override
	public void run() {
		for(int i =0;i<3;i++){
			for(int j =0 ;i< 5;j++){
				StringBuilder request = new StringBuilder();
				request.append(i);
				request.append(":");
				request.append(j);
				try {
					requestList.put(request.toString());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.printf("Client: %s at %s.\n",request,new Date());
			}
			
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
