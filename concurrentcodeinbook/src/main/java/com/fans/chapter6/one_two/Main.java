package com.fans.chapter6.one_two;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：创建100个线程添加元素，再创建100个线程取出元素
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		Thread [] threads = new Thread[100];
		for(int i =0;i<threads.length;i++){
			AddTask task = new AddTask(list);
			threads[i] = new Thread(task);
			threads[i].start();
		}
		System.out.printf("Main: %d AddTask threads have been launched\n",threads.length);
		for(int i =0;i< threads.length;i++){
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Main: Size of the list: "+list.size());
		//Thread [] threads1 = new Thread[100];

		for(int i =0 ;i<threads.length;i++){
			PollTask task = new PollTask(list);
			threads[i] = new Thread(task);
			threads[i].start();
		}
		
		System.out.printf("Main: %d PollTask threads have been lanuched\n",threads.length);
		for(int i =0 ;i<threads.length;i++){
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.printf("Main: Size of the list : %d\n",list.size());
	}
	
	
	
}
