package com.fans.chapter6.one_two;

import java.util.List;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：向集合中添加元素，添加10000个
 * 说明：
 */
public class AddTask implements Runnable{

	private List<String> list;
	
	public AddTask(List<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		for(int i =0;i<100;i++){
			list.add(name+": Element "+i);
		}
	}

}
