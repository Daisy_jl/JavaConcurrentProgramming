package com.fans.chapter6.four;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：使用带有延迟元素的线程安全列表
 * 说明：
 */
public class Event implements Delayed{

	private Date startDate;
	
	public Event(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * 该方法接收一个Delayed对象为参数，并比较两者延迟值的大小
	 */
	@Override
	public int compareTo(Delayed o) {
		long result = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
		if(result < 0){
			return  -1;
		}else if(result > 0 ){
			return 1;
		}
		return 0;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		Date now = new Date();
		long diff = startDate.getTime() - now.getTime();
		return unit.convert(diff, TimeUnit.MILLISECONDS);
	}

}
