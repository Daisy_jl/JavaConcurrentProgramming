package com.fans.chapter6.four;

import java.util.Date;
import java.util.concurrent.DelayQueue;

public class Task  implements Runnable{

	private int id;
	private DelayQueue<Event> queue;
	
	public Task(int id, DelayQueue<Event> queue) {
		this.id = id;
		this.queue = queue;
	}


	@Override
	public void run() {
		Date now = new Date();
		Date delay = new Date();
		//�����ӳ�ʱ��
		delay.setTime(now.getTime()+(id*10000));
		for(int i =0;i< 100;i++){
			Event event = new Event(delay);
			System.out.println("you have added a event to queue");
			queue.add(event);
		}
	}

}
