package com.fans.chapter6.four;

import java.util.Date;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		DelayQueue<Event> queue = new DelayQueue<Event>();
		Thread threads []= new Thread[5];
		for(int i =0; i < threads.length;i++ ){
			Task task = new Task(i+1,queue);
			threads[i] = new Thread(task);
		}
		for(int  i =0;i< threads.length;i++){
			threads[i].start();
		}
		for(int i =0 ; i<threads.length;i++){
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		do{
			int counter = 0;
			Event event;
			do{
				event = queue.poll();
				if(event != null)counter ++;
			}while(event != null);
			System.out.printf("At %s you have read %d events \n",new Date(),counter);
			try {
				TimeUnit.MILLISECONDS.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}while(queue.size() > 0);   //该方法返回列表元素中的总个数，包括活动和非活动的元素
	}
}
