package com.fans.chapter6.one;

import java.util.concurrent.ConcurrentLinkedDeque;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：从集合中取出元素
 * 说明：
 */
public class PollTask implements Runnable{

	private ConcurrentLinkedDeque<String> list;
	
	public PollTask(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		for(int  i =0;i<5000;i++){
			list.pollFirst();   //取出列表首元素
			list.pollLast();    //取出列表尾元素
		}
	}

}
