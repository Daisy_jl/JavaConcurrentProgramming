package com.fans.chapter6.one;

import java.util.concurrent.ConcurrentLinkedDeque;
/**
 * 
 * @author fcs
 * @date 2015-6-21
 * 描述：向集合中添加元素，添加10000个
 * 说明：
 */
public class AddTask implements Runnable{

	private ConcurrentLinkedDeque<String> list;
	
	public AddTask(ConcurrentLinkedDeque<String> list) {
		this.list = list;
	}

	@Override
	public void run() {
		String name = Thread.currentThread().getName();
		for(int i =0;i<10000;i++){
			list.add(name+": Element "+i);
		}
	}

}
