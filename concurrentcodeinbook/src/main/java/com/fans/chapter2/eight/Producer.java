package com.fans.chapter2.eight;
/**
 * 
 * @author fcs
 * @date 2015-4-15
 * 描述：生产者线程
 * 说明：
 */
public class Producer implements  Runnable{
	private FileMock fileMock;
	private Buffer buffer;
	public Producer(FileMock fileMock, Buffer buffer) {
		super();
		this.fileMock = fileMock;
		this.buffer = buffer;
	}
	/**
	 * 用来读取文件中的所有行，并且使用insert方法将数据插入到缓冲区中
	 * 读完数据后设置pendingLines方法通知缓冲区停止生产更多的行。
	 */
	@Override
	public void run() {
		buffer.setPendingLines(true);
		while(fileMock.hasMoreLines()){
			String line = fileMock.getLine();
			buffer.insert(line);
		}
		buffer.setPendingLines(false);
		
	}
}
