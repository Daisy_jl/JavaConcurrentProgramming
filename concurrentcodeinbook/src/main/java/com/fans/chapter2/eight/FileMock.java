package com.fans.chapter2.eight;
/**
 * 
 * @author fcs
 * @date 2015-4-15
 * 描述：在锁中使用多个条件（multiple Condition）
 * 说明：
 */
public class FileMock {
	Object bul;
    private String content [] ;  //存储文件的内容
  	private int index;           //从文件中读取内容的行号
  	/**
  	 * 使用构造方法生成需要的数据
  	 * @param size
  	 * @param length
  	 */
  	public FileMock(int size,int length){
  		content = new String[size];
  		
  		for(int i =0;i < size;i++){
  			StringBuffer buffer  = new StringBuffer(length);
  			for(int j =0 ;j< length;j++){
  				int numb = (int)Math.random() * 255;
  				buffer.append((char)numb);
  			}
  			content[i] = buffer.toString();
  		}
  		index = 0;
  	}

  	/**
  	 * 
  	 * 作者：fcs
  	 * 描述：如果文件有可以处理的数据行则返回true.如果没有可以处理的数据则返回false
  	 * 说明：
  	 * 返回：
  	 * 参数：
  	 * 时间：2015-4-15
  	 */
  	public boolean hasMoreLines(){
  		return index < content.length;
  	}
  	/**
  	 * 
  	 * 作者：fcs
  	 * 描述：返回属性index指定的行内容，并将index自动增加1
  	 * 说明：
  	 * 返回：
  	 * 参数：
  	 * 时间：2015-4-15
  	 */
  	public String getLine(){
  		if(this.hasMoreLines()){
  			System.out.println("mock: "+(content.length - index));
  			return content[index++];
  		}
  		return null;
  	}
  	
}
