package com.fans.chapter2.eight;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2015-4-15
 * 描述：消费者线程
 * 说明：
 */
public class Consumer  implements Runnable {
	private Buffer buffer;
	
	public Consumer(Buffer buffer) {
		super();
		this.buffer = buffer;
	}

	@Override
	public void run() {
		while(buffer.hasPendingLines()){
			String line = buffer.get();
			processLine(line);
		}
	}
	//休眠10毫秒，模拟数据处理
	public void processLine(String line){
		Random random  = new Random();
		try {
			Thread.sleep(random.nextInt(100));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
