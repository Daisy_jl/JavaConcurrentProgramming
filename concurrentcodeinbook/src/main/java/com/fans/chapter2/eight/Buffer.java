package com.fans.chapter2.eight;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
/**
 * 
 * @author fcs
 * @date 2015-4-16
 * 描述：缓冲区
 * 说明：
 */
public class Buffer {
	private LinkedList<String> buffer ; // 存放共享数据
	private int maxsize;    //存放buffer的长度
	private ReentrantLock  lock;  //用来对修改buffer的代码进行控制。
	private Condition lines;    //控制行数的条件
	private Condition space;    //控制是否有数据的条件
	boolean pendingLines ;      //表示缓冲区是否有数据的条件
	public Buffer(int maxsize){
		this.maxsize = maxsize;
		buffer  = new LinkedList<String>(); 
		lock  = new ReentrantLock();
		lines = lock.newCondition();
		space = lock.newCondition();
		pendingLines = true;
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：将数据插入缓冲区，当缓冲区满的时候，线程等待，当缓冲区有数据的时候唤醒线程
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-15
	 */
	public void insert(String line){
		lock.lock();   //获取锁
		try {
			while(buffer.size() == maxsize){
				space.await();   //缓冲区满的时候等待
			}
			buffer.offer(line);
			System.out.printf("%s inserted Line: %d\n",Thread.currentThread().getName(),buffer.size());
			lines.signalAll() ;  //缓冲区有数据的时候唤醒
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally{
			lock.unlock();  //释放锁
		}
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：返回缓冲区的第一个字符串，先获取锁，然后检查缓冲区是不是有数据行，如果缓冲区是空的
	 * 就调用条件lines的await()方法等待缓冲区出现数据。
	 * 当其他线程调用条件lines的signal()或者signalAll()的时候，该线程唤醒
	 * 在有数据的时候，get方法获取缓冲区的第一行，并且调用条件space的signalAll()的方法，并且返回这个数据行字符串
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-15
	 */
	public String get(){
		String line = null;
		lock.lock();
		try {
			while((buffer.size() == 0) && hasPendingLines()){
				lines.await();
			}
			if(hasPendingLines()){
				line = buffer.poll();
				System.out.printf("%s LIne readed: %d\n",Thread.currentThread().getName(),buffer.size());
			    space.signalAll();  
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
		return line;
	}
	
	/*设置pendingLines的值的，当生产者不再生产新数据行的时候调用该方法*/
	public void setPendingLines(boolean pendingLines){
		this.pendingLines = pendingLines;
	}

	/*如果有数据行返回true*/
	public boolean hasPendingLines(){
		return pendingLines || buffer.size() > 0;
	}

}
