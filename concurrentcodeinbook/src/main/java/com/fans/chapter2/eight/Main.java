package com.fans.chapter2.eight;

public class Main {
	public static void main(String[] args) {
		FileMock  fileMock = new FileMock(100, 10);
		Buffer buffer = new Buffer(20);
		Producer  producer = new Producer(fileMock, buffer);
		Thread pthread = new Thread(producer,"producer");
		
		Consumer  consumer [] = new Consumer[3];
		Thread  cthread [] = new Thread[3];
		for(int i =0;i< 3;i++){
			consumer[i] = new Consumer(buffer);
			cthread[i] = new Thread(consumer[i]);
		}
		
		pthread.start();
		
		for(int i =0;i< 3;i++){
			cthread[i].start();
		}
		
	}
}
