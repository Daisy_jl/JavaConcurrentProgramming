package com.fans.chapter2.four;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
/**
 * 
 * @author fcs
 * @date 2015-4-12
 * 描述：在同步代码中使用条件
 * 说明：生产者消费者队列同步实现
 */
public class EventStorage {
	private int maxsize;
	private List<Date>  storage;
	public EventStorage() {
		maxsize = 10;
		storage = new LinkedList<Date>();
	}
	public synchronized void set(){
		while(storage.size() == maxsize){
			try{
				wait();  //满足条件时让当前线程挂起。
			}catch(InterruptedException e){
				e.printStackTrace();
			}
			
		}
		storage.add(new Date());
		System.out.printf("Set %d \n",storage.size());
		notifyAll();  //唤醒其他等待的线程
	}
	/**
	 * 
	 * 作者：fcs
	 * 描述：注意这里必须在循环中调用wait()方法，并且不断查询while的条件
	 * 直到条件为真的时候才能继续
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-12
	 */
	public synchronized void get(){
		while(storage.size() == 0){
			try{
				wait();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
			
		}
		System.out.printf("Get : %d : %s \n",storage.size(),((LinkedList<?>)storage).poll());
		notifyAll();  //注意这里要唤醒其他线程
	}
}
