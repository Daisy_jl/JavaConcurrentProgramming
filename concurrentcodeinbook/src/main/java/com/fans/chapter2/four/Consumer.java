package com.fans.chapter2.four;

/**
 * 
 * @author fcs
 * @date 2015-4-12 描述：生产者线程 说明：
 */
public class Consumer implements Runnable {
	private EventStorage storage;

	public Consumer(EventStorage storage) {
		super();
		this.storage = storage;
	}

	@Override
	public void run() {
		for (int i = 0; i < 100000; i++) {
			storage.get();
		}
	}
}
