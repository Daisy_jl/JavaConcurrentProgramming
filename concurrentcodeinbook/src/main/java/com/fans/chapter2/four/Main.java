package com.fans.chapter2.four;

public class Main {
	public static void main(String[] args) {
		EventStorage  storage = new EventStorage();
		Producer  producer = new Producer(storage);
		Consumer  consumer = new Consumer(storage);
		Thread pthread = new Thread(producer);
		Thread cthread = new Thread(consumer);
		pthread.start();
		cthread.start();
	}
}
