package com.fans.chapter2.five;
/**
 * 
 * @author fcs
 * @date 2015-4-12
 * 描述：创建十个线程执行打印任务
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		PrintQueue  printQueue = new PrintQueue();
		Thread thread []= new Thread[1000];
		for(int i =0;i<1000;i++){
			thread[i] = new Thread(new Job(printQueue),"thread"+i);
		}
		for(int  i= 0;i<1000;i++){
			thread[i].start();
		}
	}
}
