package com.fans.chapter2.five;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author fcs
 * @date 2015-4-12
 * 描述：使用锁实现同步
 * 说明：
 */
public class PrintQueue {
	private final Lock queueLock = new ReentrantLock();
	/**
	 * 
	 * 作者：fcs
	 * 描述：模拟文档的打印
	 * 说明：获取锁之后，执行完代码要使用finally释放锁
	 * 返回：
	 * 参数：
	 * 时间：2015-4-12
	 */
	public void prontJob(Object document){
		queueLock.lock();   //调用该方法获取对锁对象的控制
		try {
			Long duration = (long) (Math.random() *10000);
			System.out.println(Thread.currentThread().getName()+"PrintQueue: printing a Job during "+(duration / 1000)+" seconds");
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			queueLock.unlock();  //通过该方法释放对锁对象的控制
		}
	}
	
}
