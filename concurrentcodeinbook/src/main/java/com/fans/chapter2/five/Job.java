package com.fans.chapter2.five;

public class Job  implements Runnable{
	private PrintQueue  printQueue;
	
	public Job(PrintQueue printQueue) {
		super();
		this.printQueue = printQueue;
	}

	@Override
	public void run() {
		System.out.printf("%s: Goging to print a document \n",Thread.currentThread().getName());
		printQueue.prontJob(new Object());
		System.out.printf("%s The document has been printed \n",Thread.currentThread().getName());
	}
}
