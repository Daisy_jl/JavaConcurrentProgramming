package com.fans.chapter7.two;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		//用来存放等待执行的任务
		ThreadPoolExecutor executor = new ThreadPoolExecutor(2,2,1,TimeUnit.SECONDS,new PriorityBlockingQueue<Runnable>());
		
		for(int i =0 ;i< 4;i++){
			MyProprityTask task = new MyProprityTask(i, "task "+i);
			executor.execute(task);
		}
		
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		for(int i  =4;i < 8;i++){
			MyProprityTask task = new MyProprityTask(i, "task "+i);
			executor.execute(task);
		}
		
		executor.shutdown();
		
		try {
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main: End of the program.");
		
	}
}
