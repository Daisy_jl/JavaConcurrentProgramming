package com.fans.chapter7.two;

import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-7-1
 * 描述：实现基于优先的Executor类
 * 说明：
 */
public class MyProprityTask implements Runnable,Comparable<MyProprityTask> {

	private int priority;
	private String name;	
	
	public MyProprityTask(int priority, String name) {
		super();
		this.priority = priority;
		this.name = name;
	}

	public int getPriority(){
		return priority;
	}
	
	
	@Override
	public int compareTo(MyProprityTask o) {
		if(this.getPriority() < o.getPriority()){
			return 1;
		}if(this.getPriority() > o.getPriority()){
			return -1;
		}
		return 0;
	}

	@Override
	public void run() {
		System.out.printf("MyPriorityTask: %s priority: %d\n",name,priority);
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
