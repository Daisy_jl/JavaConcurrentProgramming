package com.fans.chapter7.four;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import cn.fans.chapter7.three.MyTask;
import cn.fans.chapter7.three.MyThreadFactory;
/**
 * 
 * @author fcs
 * @date 2015-7-1
 * 描述：在Executor对象中使用ThreadFactory接口
 * 说明：本例中的main()方法使用newCachedThreadPool()方法创建了一个Excutor对象，并
 * 以创建的工厂对象作为传入参数，所以创建的Executor对象将使用这个工厂创建它所需要的线程，
 * 并且执行MyThread类的线程对象。
 */
public class Main {
	public static void main(String[] args) {
		MyThreadFactory threadFactory = new MyThreadFactory("MyThreadFactory");
		//新的Executor对象将使用工厂创建必须的线程，将执行MyThread线程
		ExecutorService executor = Executors.newCachedThreadPool(threadFactory);
		
		MyTask task = new MyTask();
		executor.submit(task);
		
		executor.shutdown();
		try {
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main: end of the program.\n");
	}
}
