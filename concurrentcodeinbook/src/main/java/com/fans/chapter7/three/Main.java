package com.fans.chapter7.three;


public class Main {
	public static void main(String[] args) {
		MyThreadFactory myFactory = new MyThreadFactory("MyThreadFactory");
		MyTask task = new MyTask();
		Thread thread = myFactory.newThread(task);
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main: Thread information .\n");
		System.out.printf("%s\n",thread);
		System.out.println("Main: End of the example .\n");
	}
}
