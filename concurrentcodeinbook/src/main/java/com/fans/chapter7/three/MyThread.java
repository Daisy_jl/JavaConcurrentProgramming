package com.fans.chapter7.three;

import java.util.Date;

/**
 * 
 * @author fcs
 * @date 2015-7-1
 * 描述：实现ThreadFactory接口生成定制线程
 * 说明：使用的是工厂模式，是一种创建模式，目标是创建一个类并通过这个类创建一个或多个类的对象，
 * 当创建一个类的对象时，使用工厂类而不是new操作符
 * 
 * java 提供了ThreadFactory接口来实现Thread对象工厂。Executor框架或者Fork/Join()框架，使用了
 * 线程工厂来创建线程。
 * 
 * 线程工厂在java并发API中的另一个应用是Executors类，提供了大量方法来创建不同类型的Executor对象。
 * 
 * 
 */
public class MyThread extends Thread{
	private Date creationDate;
	private Date startDate;
	private Date finishDate;
	
	public MyThread(Runnable target,String name){
		super(target,name);
		setCreationDate();
	}
	
	//实现run()方法。保存线程的开始时间，然后调用父类的run()方法，保存执行的结果。
	@Override
	public void run(){
		setStartDate();
		super.run();
		setFinishDate();
	}
	
	
	public void setCreationDate() {
		creationDate = new Date();;
	}
	public void setStartDate() {
	    startDate = new Date();
	}
	public void setFinishDate() {
		finishDate = new Date();
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：用来计算线程开始和结束的时间差
	 * 时间：2015-7-1
	 */
	public long getExecutionTime(){
		return finishDate.getTime() - startDate.getTime();
	}

	/**
	 * 覆盖toString方法，返回线程的创建时间和执行时间
	 */
	@Override
	public String toString() {
		StringBuilder buffer = new StringBuilder();
		buffer.append(getName());
		buffer.append(": ");
		buffer.append(" Creation Date: ");;
		buffer.append(creationDate);
		buffer.append(" : Running time: ");
		buffer.append(getExecutionTime());
		buffer.append(" Milliseconds");
		return buffer.toString();
	}
	
	
	
	
}
