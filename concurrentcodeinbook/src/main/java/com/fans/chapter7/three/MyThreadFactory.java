package com.fans.chapter7.three;

import java.util.concurrent.ThreadFactory;
/**
 * 
 * @author fcs
 * @date 2015-7-1
 * 描述: 实现线程工厂接口，生成定制的线程对象
 * 说明：
 */
public class MyThreadFactory  implements ThreadFactory{
	
	private int counter;
	private String prefix;
	
	public MyThreadFactory(String prefix){
		this.prefix = prefix;
		counter = 1;
	}
	//创建Thread对象，并添加counter属性
	@Override
	public Thread newThread(Runnable r) {
		MyThread myThread = new MyThread(r,prefix+"-"+counter);
		counter ++;
		return myThread;
	}

}
