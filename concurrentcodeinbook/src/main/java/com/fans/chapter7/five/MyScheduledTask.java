package com.fans.chapter7.five;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Delayed;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-7-1
 * 描述：定制运行在定时线程池中的任务
 * 说明：该类即可以执行延迟任务，也可以执行周期性任务
 * 如果执行的是一个延迟任务：getDelay()方法返回传入任务的延迟值。
 * 如果执行的是一个周期性的任务： getDelay()方法返回startDate属性与当前的时间差
 * 
 * 注： 在执行器已经关闭的情况下，周期性任务不需要再加入到执行器队列中。
 * 
 */
public class MyScheduledTask<V> extends FutureTask<V> implements RunnableScheduledFuture<V> {

	
	private RunnableScheduledFuture<V> task;  //task任务
		
	private ScheduledThreadPoolExecutor executor;  //周期线程执行器
	private long period;    //周期执行的时间
	private long startDate; //开始时间

	//接收Runnable任务类型的对象
	public MyScheduledTask(Runnable runnable,V result,
			RunnableScheduledFuture<V> task,
			ScheduledThreadPoolExecutor executor) {
		super(runnable,result);
		this.task = task;
		this.executor = executor;
	}


	public MyScheduledTask(Callable<V> callable) {
		super(callable);
	}

	/**
	 * 如果是周期性的任务，并且startDate属性值不等于0，则计算startDate属性和当前时间的时间差作为返回值
	 * 否则返回存放在task属性中的延迟值。注意，返回结果需要被转换为参数指定的时间单位。
	 */
	@Override
	public long getDelay(TimeUnit unit) {
		if(!isPeriodic()){
			return task.getDelay(unit);
		}else{
			if(startDate == 0){
				return task.getDelay(unit);
				
			}else{
				Date now = new Date();
				long delay = startDate - now.getTime();
				return unit.convert(delay, TimeUnit.MILLISECONDS);
				
			}
		}
	}

	@Override
	public int compareTo(Delayed o) {
		return task.compareTo(o);
	}

	//调用原来任务的isPeriodic()方法
	@Override
	public boolean isPeriodic() {
		return  task.isPeriodic(); 
	}
	
	//如果是周期性任务，则需要用任务下一次执行的开始时间更新它的startDate()属性，即用当前时间加上
	//周期间隔作为下一次执行的开始时间。然后再次将任务添加到ScheduledThreadExecutor对象的队列中。
	@Override
	public void run() {
		if(isPeriodic() && (!executor.isShutdown())){
			Date now = new Date();
			startDate = now.getTime()+period;
			executor.getQueue().add(this);
		}
		
		System.out.printf("Pre-MyScheduledTask: %s\n",new Date());
		System.out.printf("MyScheduledTask: Is Periodic: %S\n",isPeriodic());
		super.runAndReset();
		System.out.printf("Post-MyScheduledTask: %s\n",new Date());
		
	}
	
	//设置任务周期
	public void setPeriod(long period){
		this.period = period;
	}
}
