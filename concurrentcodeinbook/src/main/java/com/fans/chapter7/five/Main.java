package com.fans.chapter7.five;

import java.util.Date;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author fcs
 * @date 2015-7-1
 * 描述：该类创建了一个MyScheduledThreadPoolExecutor执行器对象并发送给下面的两个任务
 * 一个延迟任务，在当前时间1秒后执行
 * 一个周期任务，第一次在当前时间1秒后执行，接下来每3秒执行一次
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		MyScheduledThreadPoolExecutor executor = new MyScheduledThreadPoolExecutor(4);
		Task task = new Task();
		System.out.printf("Main: %s\n",new Date());
		//使用该方法发送一个延迟任务到执行器，这个任务将延迟1秒后被执行
		executor.schedule(task, 5, TimeUnit.SECONDS);
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		task = new Task();
		System.out.printf("Main: %s\n",new Date());
		//1:开始执行的延迟时间，5:每隔1秒周期性的创建一个任务
		executor.scheduleAtFixedRate(task, 1, 1, TimeUnit.SECONDS);
		
		try {
			
			//主线程休眠10秒
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		executor.shutdown();
		try {
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main: End of the program.\n");
		
	}
	
}
