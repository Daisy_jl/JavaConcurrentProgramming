package com.fans.chapter7.five;

import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-7-1
 * 描述：自定义ScheduledThreadPoolExecutor类
 * 说明：用来执行MyScheduledTask任务
 */
public class MyScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor {

	
	public MyScheduledThreadPoolExecutor(int corePoolSize) {
		super(corePoolSize);
	}

	/**
	 * 接收将要被执行的Runnable对象作为参数，创建MyScheduledTask任务
	 */
	@Override
	protected <V> RunnableScheduledFuture<V> decorateTask(Runnable runnable,
			RunnableScheduledFuture<V> task) {
		MyScheduledTask<V> myTask = new MyScheduledTask<V>(runnable,null,task,this);
		return myTask;
	}

	//调用父类的方法，将返回的对象强制转换成MyScheduledTask对象，使用setPeriod()方法设置任务的周期。
	@Override
	public ScheduledFuture<?> scheduleAtFixedRate(Runnable command,
			long initialDelay, long period, TimeUnit unit) {
		ScheduledFuture<?> task = super.scheduleAtFixedRate(command, initialDelay, period, unit);
		MyScheduledTask<?> mytask = (MyScheduledTask<?>)task;
		mytask.setPeriod(TimeUnit.MILLISECONDS.convert(period, unit));
		return task;
	}
}
