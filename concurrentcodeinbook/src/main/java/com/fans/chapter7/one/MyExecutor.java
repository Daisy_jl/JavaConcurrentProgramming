package com.fans.chapter7.one;

import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-6-29
 * 描述：定制ThreadPoolFactory
 * 说明：
 */
public class MyExecutor extends ThreadPoolExecutor{

	private ConcurrentHashMap<String,Date> startTimes;

	public MyExecutor(int corePoolSize, int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
		startTimes = new ConcurrentHashMap<String, Date>();
	}
	
	/**
	 * 重写关闭执行器方法，将已经执行过的认为，正在执行的任务，和等待执行的任务的信息输出到控制台上。
	 * 使用super关键字调用父类的shutdown()方法
	 */
	@Override
	public void shutdown() {
		System.out.println("MyExecutor: Going to shutdown.");
		System.out.printf("MyExecutor: Executed Tasks: %d\n",getCompletedTaskCount());
		System.out.printf("MyExecutor: Running Tasks: %d\n",getActiveCount());
		System.out.printf("MyExecutor: Pending tasks: %d\n",getQueue().size());
	}
	
	/**
	 * 重写shutdownNow()方法，将已经执行过的任务，正在执行的任务和等待执行的任务的信息输出到控制台中。
	 */
	@Override
	public List<Runnable> shutdownNow() {
		System.out.println("MyExecutor: Going to immediately shutdown.");
		
		//获得已经执行过的任务数量
		System.out.printf("MyExecutor: Executed tasks: %d\n",getCompletedTaskCount());
		//获得正在执行的任务数量
		System.out.printf("MyExecutor: Running tasks: %d\n",getActiveCount());
		//通过阻塞队列的size()方法获得等待执行任务的数量
		System.out.printf("MyExecutor: Pending tasks: %d\n",getQueue().size());
		return super.shutdownNow();
	}

	/**
	 * 重写beforeExecute()方法，输出将要执行的线程的名字，任务的哈希码，
	 * 开始日期放到HashMap中，是以任务的哈希码值
	 * 作为主键。
	 */
	@Override
	protected void beforeExecute(Thread t, Runnable r) {
		System.out.printf("A task is beginning : %s: %s\n",t.getName(),r.hashCode());
		startTimes.put(String.valueOf(r.hashCode()), new Date());
	}

	/**
	 * 重写afterExecute()方法。将任务的执行结果输出到控制台，
	 * 用当前时间减去存放在并发HashMap中的起始日期来计算任务的运行时间。
	 */
	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		Future<?> result = (Future<?>)r;
		try {
			System.out.println("***************************");
			System.out.printf("MyExecutor: A task is finishing\n");
			System.out.printf("MyExecutor: Result: %s\n",result.get());
			Date startDate = startTimes.remove(String.valueOf(r.hashCode()));
			Date finishDate = new Date();
			long diff = finishDate.getTime() - startDate.getTime();
			System.out.printf("MyExecutor: Duration: %d \n",diff);
			System.out.println("***************************");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

}
