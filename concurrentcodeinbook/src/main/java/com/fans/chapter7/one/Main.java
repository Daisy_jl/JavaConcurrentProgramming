package com.fans.chapter7.one;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		MyExecutor myExecutor = new MyExecutor(2, 4, 1000,TimeUnit.MILLISECONDS , new LinkedBlockingDeque<Runnable>());
		List<Future<String>>  results = new ArrayList<Future<String>>();
		for(int  i =0 ;i< 10;i++){
			SleepTwoSecondsTask  task = new SleepTwoSecondsTask();
			Future<String>  result = myExecutor.submit(task);
			results.add(result);
		}
		for(int i =0 ;i<5;i++){
			try {
				String result = results.get(i).get();
				System.out.printf("Main: Result for Tasks: %d: %s\n",i,result);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			
		}
		myExecutor.shutdown();
		for(int i = 5;i<10;i++){
			try {
				String result = results.get(i).get();
				System.out.printf("Main: Result for Task %d : %s\n",i,result);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			
		}
		//�ȴ�ִ�������
		try {
			myExecutor.awaitTermination(1,TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Main: End of the program.");
		System.out.println(myExecutor.isTerminated()+"----");
	}
}
