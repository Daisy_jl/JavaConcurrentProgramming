package com.fans.chapter7.one;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-6-29
 * 描述：使当前线程休眠2s,然后将日期转换为字符串，并返回。
 * 说明：
 */
public class SleepTwoSecondsTask implements Callable<String>{

	public String call() throws Exception {
		TimeUnit.SECONDS.sleep(2);
		return new Date().toString();
	}

}
