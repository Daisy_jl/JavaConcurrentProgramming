package com.fans.chapter3.two;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author fcs
 * @date 2015-4-19
 * 描述：资源的多副本的并发访问控制
 * 
 * 说明：修改one包中的部分代码即可
 * 这里使用信号量的机制实现对多个资源的并发访问控制
 */
public class PrintQueue {
	private final Semaphore  semaphore;  //声明信号量
	private boolean freePrinters[];  //存放打印机的状态，即空闲或者正在打印。
	@SuppressWarnings("unused")
	private Lock lockPrinters;       //用锁的方式保护对上面数组的同步访问
	public PrintQueue(){
		semaphore = new Semaphore(3);   //信号量初始化为3，表示有三个共享资源
		freePrinters = new boolean[3];
		for(int i =0 ;i< 3;i++){
			freePrinters[i] = true;
		}
		lockPrinters = new ReentrantLock();
	}
	
	//执行打印
	public void printJob(Object object){
		try {
			semaphore.acquire();   //获得信号量，抛出异常，要捕获处理
			int assingedPrinter = getPrinter();
			long duration = (int)(Math.random()*10);
			System.out.printf("%s:  printQueue: Printing a Job in Printer %d during %d seconds\n",Thread.currentThread().getName(),assingedPrinter,duration);
			Thread.sleep(duration);   //随机等待一段时间
		
			freePrinters[assingedPrinter] = true;  //打印完毕后将该编号的打印机状态设置为可用
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			semaphore.release();   //注意没有释放信号量的话则会死锁，只有其中3个线程执行打印任务后其他线程就无法打印了
		}
	}
	//该方法查找在指定时间内可用的打印机，如果有则返回打印机序号，否则返回-1
	private int getPrinter(){
		int ret = -1;
		try {
			lockPrinters.lock();   //获取锁
			for(int i =0;i<freePrinters.length;i++){
				if(freePrinters[i]){
					ret = i;
					freePrinters[i] = false;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			lockPrinters.unlock();   //释放锁，返回获得的打印机编号
		}
		return ret;
	}
	
	
}
