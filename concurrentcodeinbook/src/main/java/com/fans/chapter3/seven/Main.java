package com.fans.chapter3.seven;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;
/**
 * 
 * @author fcs
 * @date 2015-5-2
 * 描述：生产者---消费者（一对一）问题，使用同步辅助工具实现
 * 说明：
 */
public class Main {
	public static void main(String[] args) {
		List<String> buffer1 = new ArrayList<String>();
		List<String> buffer2 = new ArrayList<String>();
		
		//创建exchanger对象，用来同步生产者和消费者
		Exchanger<List<String>>  exchanger = new Exchanger<List<String>>();
		Producer producer = new Producer(buffer1,exchanger);
		Consumer consumer = new Consumer(buffer2,exchanger);
		Thread pthread = new Thread(producer);
		Thread cthread = new Thread(consumer);
		pthread.start();
		cthread.start();
		
	}
}
