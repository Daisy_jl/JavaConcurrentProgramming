package com.fans.chapter3.seven;

import java.util.List;
import java.util.concurrent.Exchanger;
/**
 * 
 * @author fcs
 * @date 2015-5-2
 * 描述：并发任务间的数据交换
 * 说明：使用同步辅助工具Exchanger
 */
public class Consumer implements Runnable{
	private List<String> buffer;
	private final Exchanger<List<String>>  exchanger;
	
	public Consumer(List<String> buffer, Exchanger<List<String>> exchanger) {
		super();
		this.buffer = buffer;
		this.exchanger = exchanger;
	}

	@Override
	public void run() {
		int cycle = 0;
		for(int i =0;i< 10;i++){
			System.out.printf("Consumer: Cycle %d\n",cycle);
			//这里消费者要消费数据，就要先获得数据，调用exchange方法交换数据结构
			try {
				buffer = exchanger.exchange(buffer);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Consumer size: "+buffer.size());
			//将生产者放入消费者buffer列表中的10个字符串打印到控制台，并从列表中删除，保持一个空列表
			for(int j =0 ;j < 10;j++){
				String message = buffer.get(0);
				System.out.println("Consumer: "+message);
				buffer.remove(0);
			}
			cycle++;
		}
	}
}
