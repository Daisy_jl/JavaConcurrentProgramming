package com.fans.chapter3.seven;

import java.util.List;
import java.util.concurrent.Exchanger;

public class Producer implements Runnable{
	
	private List<String> buffer;   //生产者和消费者用于交换的数据结构
	
	private final Exchanger<List<String>> exchanger;  //同步生产者和消费者的交换对象
	
	public Producer(List<String> buffer, Exchanger<List<String>> exchanger) {
		super();
		this.buffer = buffer;
		this.exchanger = exchanger;
	}


	@Override
	public void run() {
		int cycle = 1;
		for(int i =0;i< 10;i++){
			System.out.printf("Produceer: Cycle %d\n",cycle);
			for(int j =0 ;j < 10;j++){
				String message = "Event" +((i * 10)+j);
				System.out.printf("Producer :%s\n",message);
				buffer.add(message);
			}
			try{
				buffer = exchanger.exchange(buffer);   //该方法会抛出异常
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("Producer: "+buffer.size());
		}
	}
}
