package com.fans.chapter3.six;

import java.util.concurrent.Phaser;
/**
 * 
 * @author fcs
 * @date 2015-5-1
 * 描述：并发阶段任务中的阶段切换
 * 说明：继承Phaser对象，并重写onAdvance对象
 */
public class MyPhaser extends Phaser{
	/**
	 * 在每个阶段任务完成后，进入下一个阶段时会执行该方法
	 * 可以处理阶段性要处理的事情
	 */
	@Override
	protected boolean onAdvance(int phase, int registeredParties) {
		switch(phase){
			case 0:
				return studentsArrived();
			case 1:
				return finishFirstExersize();
			case 2:
				return finishSecondExersize();
			case 3:
				return finishExam();
				default :
					return true;
		}
	}
	
	private boolean studentsArrived(){
		System.out.printf("Phaser: The exam are going to start. The Students are ready");
		System.out.printf("Phaser: We have %d students.\n",getRegisteredParties());
		return false;
	}
	
	private boolean finishFirstExersize(){
		System.out.println("Phaser: All the students have finished the first exersie.\n");
		System.out.println("Phaser: It's time for the second one\n");
		return false;
	}
	
	private boolean finishSecondExersize(){
		System.out.println("Phaser: All the students have finished the second exersie.\n");
		System.out.println("Phaser: It's time for the second one\n");
		return false;
	}
	
	private boolean finishExam(){
		System.out.println("Phaser: All the students have finished the exam.\n");
		System.out.println("Phaser: Thank you for your time\n");
		return true;
	}
}
