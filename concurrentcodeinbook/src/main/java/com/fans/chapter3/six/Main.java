package com.fans.chapter3.six;


public class Main {
	public static void main(String[] args) {
		MyPhaser myPhaser = new MyPhaser();
		Student [] student = new Student[5];
		for(int i =0 ;i< student.length;i++){
			student[i] = new Student(myPhaser);
			//该方法并没有建立学生对象或者它对应的执行线程与phaser之间的关联。
			//实际上phaser中的参与者数目只是一个数字，phaser与参与者不存在任何关联。
			myPhaser.register();   //通过该方法将student线程对象注册到phaser对象中
		}
		
		Thread threads [] = new Thread[student.length];
		//创建5个线程
		for(int i =0 ;i < student.length;i++){
			threads[i] = new Thread(student[i],"student"+i);
			threads[i].start();
		}
		
		//等待线程终止
		for(int i =0 ;i< student.length;i++){
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.printf("Main : The phaser has finished: %s \n",myPhaser.isTerminated());
	}
}
