package com.fans.chapter3.four;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
/**
 * 
 * @author fcs
 * @date 2015-5-2
 * 描述：CyclicBarrier类会自动维护线程的状态
 * 说明：
 */
public class Searcher implements Runnable{

	private int firstRow;
	private int lastRow;
	private MatrixMock mock;
	private Result result;
	private int number;
	private final CyclicBarrier barrier;
	
	
	
	public Searcher(int firstRow, int lastRow, MatrixMock mock, Result result,
			int number, CyclicBarrier barrier) {
		
		this.firstRow = firstRow;
		this.lastRow = lastRow;
		this.mock = mock;
		this.result = result;
		this.number = number;
		this.barrier = barrier;
	}
	/**
	 * 查找数字，使用内部变量counter来存放每行找到的次数
	 */
	@Override
	public void run() {
		int counter ;
		System.out.printf("%s: processing lines from %d to %d.\n",Thread.currentThread().getName(),firstRow,lastRow);
		for(int i = firstRow;i< lastRow;i++){
			int row [] = mock.getRow(i);
			counter =0 ;
			for(int j = 0;j<row.length;j++){
				if(row[j] == number){
					counter++;
				}
			}
			result.setData(i, counter);
		}
		
		System.out.printf("%s Lines processed\n.",Thread.currentThread().getName());
		try {
			/*每个线程到达集合点后就会调用await()方法通知CyclicBarrier对象，该对象会让这个线程休眠直到其他所有线程都到达集合点*/
			barrier.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}
	}
}
