package com.fans.chapter3.four;

import java.util.Random;

/**
 * 
 * @author fcs
 * @date 2015-4-21
 * 描述：在集合点的同步
 * 说明：使用CyclicBarrier类（线程等待计数器）
 * 使用多个线程在一个大的矩阵中查找指定数字出现的次数
 */
public class MatrixMock {
	  private int data[][];
	  /**
	   * 
	   * @param size    //矩阵的行数
	   * @param length  //每行的长度
	   * @param number  //要寻找的数字
	   */
	  public MatrixMock(int size,int length,int number){
		  int counter = 0;
		  data = new int [size][length];
		  Random random  = new Random();
		  for(int i = 0;i < size;i++){
			  for(int j =0 ;j< length;j++){
				  data[i][j] = random.nextInt(10);
				  if(data[i][j] == number){
					  counter++;
				  }
			  }
		  }
		  System.out.printf("Mock : There are %d ocurrences of number in generated data.\n",counter,number);
	  }
	  
	  /**
	   * 
	   * 作者：fcs
	   * 描述：返回矩阵中指定行的内容，如果没有就返回null
	   * 参数：row 行号
	   * 时间：2015-4-21
	   */
	  public int [] getRow(int row){
		  if((row>=0) &&(row < data.length)){
			  return data[row];
		  }
		  return null;
	  }
	  
}
