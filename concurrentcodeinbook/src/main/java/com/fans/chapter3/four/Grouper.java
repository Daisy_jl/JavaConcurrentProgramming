package com.fans.chapter3.four;

public class Grouper implements Runnable {
	private Result result;
	
	public Grouper(Result result) {
		super();
		this.result = result;
	}

	/**
	 * 计算在结果类数组中查找的次数
	 */
	@Override
	public void run() {
		int finalResult  = 0;
		System.out.println("Grouper: Processing results...\n");
		int data [] = result.getData();
		for(int number:data){
			finalResult +=number;
		}
		System.out.printf("Grouper:Total result : %d.\n",finalResult);
	}
}
