package com.fans.chapter3.four;

import java.util.concurrent.CyclicBarrier;

public class Main {
	public static void main(String[] args) {
		final int ROWS = 10000;         //行数
		final int NUMBERS = 1000;       //随机数范围
		final int SEARCH = 5;  			//要搜索的数字
		final int PARTICIPANT = 5;		//搜索线程的数量
		final int LINES_PARTICIPANT = 2000;  //每个线程搜索的区间
		MatrixMock mock = new MatrixMock(ROWS, NUMBERS, SEARCH);
		Result result = new Result(ROWS);
		
		Grouper grouper = new Grouper(result);
	
		//创建CyclicBarrier类，这个对象将等待五个线程运行结束，
		//然后执行创建的Grouper线程对象
		CyclicBarrier barrier = new CyclicBarrier(PARTICIPANT,grouper);
		Searcher searcher [] = new Searcher[PARTICIPANT];
		for(int i =0;i < PARTICIPANT;i++){
			searcher [i] = new Searcher(i*LINES_PARTICIPANT, (i*LINES_PARTICIPANT)+LINES_PARTICIPANT, mock, result, 5, barrier);
			Thread thread = new Thread(searcher[i]);
			thread.start();
		}
		System.out.println("Main: The main thread has finished.\n");
	}
}
