package com.fans.chapter3.four;
/**
 * 
 * @author fcs
 * @date 2015-4-21
 * 描述：保存矩阵中每行查找到指定数字的次数
 * 说明：
 */
public class Result {
	private int data[];

	//初始化结果数组
	public Result(int size) {
		data = new int [size];
	}

	public int[] getData() {
		return data;
	}

	//设置指定数组索引的值，存放矩阵中指定行查找的数字的个数
	public void setData(int position,int value) {
		data[position] = value;
	}
	
}
