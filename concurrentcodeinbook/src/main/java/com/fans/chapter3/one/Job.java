package com.fans.chapter3.one;
/**
 * 
 * @author fcs
 * @date 2015-4-19
 * 描述：工作打印类
 * 说明：将打印文档发送到打印机
 */
public class Job  implements Runnable{

	private PrintQueue printQueue;
	//使用构造方法初始化打印队列
	public Job(PrintQueue printQueue){
		this.printQueue = printQueue;
	}
	//调用打印方法，执行打印任务，将打印信息输出到控制台
	@Override
	public void run() {
		System.out.printf("%s: Going to print a job\n",Thread.currentThread().getName());
		printQueue.printJob(new Object());
		System.out.printf("%s The document has been printed\n",Thread.currentThread().getName());
	}
}
