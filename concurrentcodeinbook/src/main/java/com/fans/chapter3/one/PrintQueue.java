package com.fans.chapter3.one;

import java.util.concurrent.Semaphore;

/**
 * 
 * @author fcs
 * @date 2015-4-19
 * 描述：资源的并发访问控制
 * 说明：信号量机制
 */
public class PrintQueue {
  private final Semaphore semaphore;
  
  //对信号量进行初始化
  public PrintQueue(){
	  semaphore = new Semaphore(1,true);   //true表示使用非公平模式竞争信号量保护的资源,默认使用公平模式
  }
  /**
   * 
   * 作者：fcs
   * 描述：打印方法，使用信号量进行资源的并发访问的控制
   * 说明：
   * 返回：
   * 参数： document 传入打印的文档
   * 时间：2015-4-19
   */
  public void printJob(Object document){
	  try {
		semaphore.acquire();    //获得信号量，该方法会抛出异常
		long duration = (long)(Math.random()*10);
		System.out.printf("%sL PrintQueue: print a Job during %d seconds",Thread.currentThread().getName(),duration);
		Thread.sleep(duration);   //随机睡眠duration时间
	} catch (InterruptedException e) {
		e.printStackTrace();
	}finally{
		semaphore.release();     //释放信号量,当该代码被注释后，运行会出现死锁，导致线程饥饿，所以获取信号量后，完成任务一定要释放信号量
	}
  }
}
