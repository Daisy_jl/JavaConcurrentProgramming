package com.fans.chapter3.one;
/**
 * 
 * @author fcs
 * @date 2015-4-19
 * 描述：创建十个线程执行打印任务，但是共享一个打印机，
 * 并且使用信号量机制实现资源的并发访问控制
 * 
 * 这里使用的是多线程并发竞争的公平模式，所以JVM会按照等待时间最长的线程
 * 来选择执行，可以信号量的构造函数中传入参数进行修改为非公平模式。
 * 
 * 说明：
 */
public class Main {
   public static void main(String[] args) {
	   PrintQueue printQueue = new PrintQueue();
	   
	  Thread [] thread = new Thread[10];
	  for(int i =0 ;i< 10;i++){
		  thread[i] = new Thread(new Job(printQueue),"Thread"+i);  
	  }
	  
	  for(int i = 0;i<10;i++){
		  thread[i].start();
	  }
   
   }	
}
