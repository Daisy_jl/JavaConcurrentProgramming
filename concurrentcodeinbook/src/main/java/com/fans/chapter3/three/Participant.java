package com.fans.chapter3.three;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

/**
 * 
 * @author fcs
 * @date 2015-4-19
 * 描述：创建与会者类，实现Runnable接口
 * 说明：
 */
public class Participant implements Runnable{

	private Videoconference  videoconference;
	private String name;
	public Participant(Videoconference videoconference,String name){
		this.videoconference = videoconference;
		this.name = name;
	}
	
	@Override
	public void run() {
		long duration = (long)(Math.random() * 10);
		try {
			TimeUnit.SECONDS.sleep(duration);
			videoconference.arrive(name);   //调用视频会议对象的arrive()方法，表明一个与会者的到来
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
