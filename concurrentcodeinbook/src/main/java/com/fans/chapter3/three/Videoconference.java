package com.fans.chapter3.three;

import java.util.concurrent.CountDownLatch;

/**
 * 
 * @author fcs
 * @date 2015-4-19
 * 描述： 等待多个并发事件的完成
 * 说明：可以使用countDownLacth实现，该类是同步辅助类
 * 
CountDownLatch类是一个同步计数器,构造时传入int参数,该参数就是计数器的初始值，每调用一次countDown()方法，计数器减1,计数器大于0 时，await()方法会阻塞程序继续执行
CountDownLatch如其所写，是一个倒计数的锁存器，当计数减至0时触发特定的事件。利用这种特性，可以让主线程等待子线程的结束。下面以一个模拟运动员比赛的例子加以说明。
*/

//创建视频会议类
public class Videoconference implements Runnable{
	private final CountDownLatch controller;
	
	//初始化倒数计数器
	public Videoconference(int number){
		controller = new CountDownLatch(number);
	}
	@Override
	public void run() {
		System.out.printf("VideoConference: Initialization: %d participants.\n",controller.getCount());
		try {
			controller.await();   //使用该方法等待所有与会者到达，相当于所有线程都在同一个地点到达，该方法会抛出异常，进行捕获处理
			System.out.printf("VideoConference: All Participants have come\n");
			System.out.printf("VideoConference: Let's start ... \n");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：与会者进入视频会议的时候，这个方法将被调用
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-19
	 */
	public void arrive(String name){
		System.out.printf("%s has arrived .\n",name);
		controller.countDown();   //内部计数器将减一
		System.out.printf("VideoConference: Waiting for %d participants.\n",controller.getCount());
	}
}
