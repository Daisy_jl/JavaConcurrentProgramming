package com.fans.chapter3.three;

public class Main {
	public static void main(String[] args) {
		//创建视频会议对象，等待10个与会者到来
		Videoconference  videoconference = new Videoconference(10);
		Thread thread = new Thread(videoconference);
		thread.start();
		for(int i =0;i< 10;i++){
			Participant participant = new Participant(videoconference, "Participant"+i);
			Thread th = new Thread(participant);
			th.start();
		}
	}
}
