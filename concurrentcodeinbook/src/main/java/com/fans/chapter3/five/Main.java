package com.fans.chapter3.five;

import java.util.concurrent.Phaser;

public class Main {
	public static void main(String[] args) {
		Phaser phaser = new Phaser(3);
		FileSearch system = new FileSearch("c:\\windows", "log", phaser);
		
		FileSearch apps = new FileSearch("c:\\Program Files", "log", phaser);
		
		FileSearch documents = new  FileSearch("c:\\Documents And Settings","log",phaser);
		
		Thread systemThread = new Thread(system,"system");
		systemThread.start();
		
		Thread appsThread = new Thread(apps,"apps");
		appsThread.start();
		
		Thread documentsThread = new Thread(apps,"documents");
		
		documentsThread.start();
		
		try {
			systemThread.join();
			appsThread.join();
			documentsThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
