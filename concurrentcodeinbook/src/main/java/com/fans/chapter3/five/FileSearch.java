package com.fans.chapter3.five;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

public class FileSearch implements Runnable{

	private String initPath;  //存储查找的文件夹
	
	private String end;   //存储查找文件的扩展名
	
	private Phaser phaser;  //用来控制任务不同阶段的同步
	
	private List<String> results;
	
	public FileSearch(String initPath, String end, Phaser phaser) {
		this.initPath = initPath;
		this.end = end;
		this.phaser = phaser;
		results = new ArrayList<String>();
	}

	/**
	 * 
	 * 作者：fcs
	 * 描述：处理文件夹的所有文件夹和子文件夹，对于每个文件夹这个方法将递归调用，对于每个文件，这个方法
	 * 将调用fileProcess()方法
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-23
	 */
	private void directoryProcess(File file){
		File list [] =  file.listFiles();
		
		if(list != null){
			for(int i = 0;i < list.length;i++){
				if(list[i].isDirectory()){
					directoryProcess(list[i]);
				}else{
					fileProcess(list[i]);
				}
			}
		}
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：查找传入文件的对象的扩展名是不是我们指定的。
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-23
	 */
	private void fileProcess(File file){
		if(file.getName().endsWith(end)){
			results.add(file.getAbsolutePath());
		}
	}
	/**
	 * 
	 * 作者：fcs
	 * 描述：对第一个阶段查找到的文件列表进行过滤，将不是过去24小时修改过的文件删除，
	 * 
	 * 说明：
	 * 返回：
	 * 参数：
	 * 时间：2015-4-23
	 */
	private void filterResult(){
		List<String> newResult = new ArrayList<String>();
		long actualDate = new Date().getTime();
		
		for(int i = 0;i < results.size();i++){
			File file = new File(results.get(i));
			long fileDate = file.lastModified();
			
			if(actualDate - fileDate <TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)){
				newResult.add(results.get(i));
			}
			
		}
		results = newResult;
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：将在第一阶段和第二阶段结束的时候被调用
	 * 用来检查结果集是不是空的
	 * 说明：如果是空的则调用phaser.arriveAndDeregister()方法
	 * 
	 * 返回：
	 * 参数：
	 * 时间：2015-4-23
	 */
	public boolean checkResult(){
		if(results.isEmpty()){
			System.out.printf("%s: phase %d: 0 results.\n",Thread.currentThread().getName(),phaser.getPhase());
			System.out.printf("%s: Phase %d End.\n",Thread.currentThread().getName(),phaser.getPhase());
			phaser.arriveAndDeregister();
			return false;
		}else{
			System.out.printf("%s:Phase %d: %d results.\n",Thread.currentThread().getName(),phaser.getPhase(),results.size());
			/*通知phaser对象当前线程已经完成当前阶段，需要被阻塞直到其他线程也都完成当前阶段*/
			phaser.arriveAndAwaitAdvance();
			return true;
		}
	}
	
	/**
	 * 
	 * 作者：fcs
	 * 描述：
	 * 说明：将结果集元素打印到控制台
	 * 返回：
	 * 参数：
	 * 时间：2015-4-23
	 */
	private void showInfo(){
		for(int i =0 ; i < results.size();i++){
			File file = new File(results.get(i));
			System.out.printf("%s: %s\n.",Thread.currentThread().getName(),file.getAbsolutePath());
		}
	}
	
	@Override
	public void run() {
		phaser.arriveAndAwaitAdvance();  //调用该方法等待所有线程都被创建后再开始
		System.out.printf("%s: Starting .\n",Thread.currentThread().getName());
		
		File file = new File(initPath);
		if(file.isDirectory()){
			directoryProcess(file);
		}
		//并发任务的第一阶段
		//使用该方法检查结果集是不是空的，如果是结束对应线程，并且用关键字return
		if(!checkResult()){
			return;
		}
		
		//并发任务的第二阶段
		//使用该方法过滤结果集
		filterResult();
		
		if(!checkResult()){
			return;
		}
		//并发任务的第三阶段
		//使用该方法将最终的结果集打印到控制台，并且撤销线程的注册，然后将线程完成的信息打印到控制台
		showInfo();
		phaser.arriveAndDeregister();
		System.out.printf("%s: work complete .\n",Thread.currentThread().getName());
	}	
}
