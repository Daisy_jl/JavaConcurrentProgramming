package com.fans.chapter4.seven;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		System.out.printf("Main: Starting at: %s\n",new Date());
		Task task = new Task("Task");
		/**
		 * 参数说明
		 * 1.要执行的任务
		 * 2.首次执行的延迟时间
		 * 3.连续执行之间的周期
		 * 4. 2,3时间参数的单位
		 */
		ScheduledFuture<?>  result = executor.scheduleAtFixedRate(task, 1, 2,TimeUnit.SECONDS);
		
		for(int i =0 ;i< 10;i++){
			System.out.printf("Main: Delay: %d\n",result.getDelay(TimeUnit.MILLISECONDS));
			try {
				TimeUnit.MILLISECONDS.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		executor.shutdown();
		
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.printf("Main: Finished at: %s\n",new Date());
	}
}
