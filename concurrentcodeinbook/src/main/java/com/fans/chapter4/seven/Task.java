package com.fans.chapter4.seven;

import java.util.Date;
/**
 * 
 * @author fcs
 * @date 2015-5-6
 * 描述：在执行器中周期性执行任务
 * 说明：使用ScheduledThreadPoolExecutor类来周期性的执行任务
 */
public class Task implements Runnable{

	private String name;
	
	public Task(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		System.out.printf("%s:Starting at: %s\n",name,new Date());
	}
}
