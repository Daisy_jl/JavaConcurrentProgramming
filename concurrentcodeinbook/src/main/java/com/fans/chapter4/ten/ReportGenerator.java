package com.fans.chapter4.ten;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-5-8
 * 描述：在执行器中分离任务的启动与结果的处理
 * 说明：使用需求：需要在一个对象里发送任务给执行器，然后在另一个对象里处理结果
 */
public class ReportGenerator implements Callable<String>{
	private String sender;
	private String title;
	public ReportGenerator(String sender, String title) {
		this.sender = sender;
		this.title = title;
	}
	
	@Override
	public String call() throws Exception {
		
		long duration = (long)(Math.random()*10);
		System.out.printf("%s_%s: ReportGenerator: Generating a report during %d seconds\n",this.sender,this.title,duration);;
		TimeUnit.SECONDS.sleep(duration);
		String ret = sender+":"+title;
		return ret;
	}
}
