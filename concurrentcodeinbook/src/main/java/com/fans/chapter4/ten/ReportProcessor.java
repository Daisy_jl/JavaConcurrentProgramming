package com.fans.chapter4.ten;

import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author fcs
 * @date 2015-6-16
 * 描述：该类将获取到ReportGenerator任务的结果
 */
public class ReportProcessor implements Runnable{
	private CompletionService<String> service;
	private boolean end;
	public ReportProcessor(CompletionService<String> service) {
		this.service = service;
		end = false;
	}
	
	@Override
	public void run() {
		while(!end){
			try {
				//调用CompletionService的poll()方法来获取下一个已经完成任务的Future对象
				Future<String> result = service.poll(20, TimeUnit.SECONDS);
				if(result != null){
					String report;
					     //使用该方法获取任务的结果。
						report = result.get();
					
					System.out.printf("ReportReceiver: Report Received: %s\n",report);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("ReportSender: End\n");
		}
	}
	
	public void setEnd(boolean end){
		this.end = end;
	}
	
}