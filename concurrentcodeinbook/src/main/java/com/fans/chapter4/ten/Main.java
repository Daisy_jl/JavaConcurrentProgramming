package com.fans.chapter4.ten;

import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		ExecutorService  executorService = (ExecutorService)Executors.newCachedThreadPool();
		CompletionService<String> service = new ExecutorCompletionService<String>(executorService);
		
		//创建两个ReportRequest对象，然后创建两个线程Thread对象分别执行他们
		ReportRequest  faceRequest = new ReportRequest("Face", service);
		ReportRequest onlineRequest = new ReportRequest("Online",service);
		
		Thread faceThread = new Thread(faceRequest);
		Thread onlineThread = new Thread(onlineRequest);
		ReportProcessor processor = new ReportProcessor(service);
		Thread senderThread = new Thread(processor);
		System.out.println("Main: Starting the Threads\n");
		
		faceThread.start();
		onlineThread.start();
		senderThread.start();
		
		System.out.println("Main: Waiting for the report generators.\n");
		//等待ReportRequest线程的结束
		try {
			faceThread.join();
			onlineThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.printf("Main: Shutting down the executor.\n");
		executorService.shutdown();
		try {
			//调用该方法等待所有的任务执行结束
			executorService.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		processor.setEnd(true);
		System.out.println("Main: Ends");
		
	}
}
