package com.fans.chapter4.ten;

import java.util.concurrent.CompletionService;

/**
 * 
 * @author fcs
 * @date 2015-5-9
 * 描述：模拟请求报告
 */
public class ReportRequest implements Runnable {
	private String name;
	
	private CompletionService<String>  service;

	public ReportRequest(String name, CompletionService<String> service) {
		this.name = name;
		this.service = service;
	}
	
	/**
	 * 调用service的submi()方法将ReportGenerator对象发送给CompletionService对象
	 */
	@Override
	public void run() {
		ReportGenerator  reportGenerator = new ReportGenerator(name, "Report");
		service.submit(reportGenerator);
	
	}
}
