package com.fans.chapter4.eleven;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {
	public static void main(String[] args) {
		RejectedTaskController  controller = new RejectedTaskController();
		ThreadPoolExecutor  executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		//设置用于被拒绝的任务的处理程序
		executor.setRejectedExecutionHandler(controller);
		System.out.printf("Main: Starting.\n");
		for(int i =0;i< 3;i++){
			Task task = new Task("Task_"+i);
			executor.submit(task);
		}
		System.out.println("Main:Shutting down the executor.\n");
		executor.shutdown();
		
		System.out.println("Main: Sending another Task\n");
		
		//关闭执行器后再发送一个任务给执行器
		Task task = new Task("RejectedTask");
		executor.submit(task);
		
		System.out.println("Main: End.");
		
	}
}
