package com.fans.chapter4.eleven;

import java.util.concurrent.TimeUnit;

public class Task implements Runnable{
	private String name;

	public Task(String name) {
		this.name = name;
	}
	
	@Override
	public void run() {
		System.out.println("Task "+name+ ": starting");
		long duration = (long)(Math.random() *10);
		System.out.printf("Task %s: ReportGenerator: Generatint a report during %d seconds\n",name,duration);
		try {
			TimeUnit.SECONDS.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("Task %s: Ending \n",name);
	}

	@Override   //重写toString方法，返回任务的名称
	public String toString() {
		return name;
	}
}
