package com.fans.chapter4.eleven;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
/**
 * 
 * @author fcs
 * @date 2015-5-9
 * 描述：处理在执行器中被拒绝的任务
 * 说明：当我们想结束执行器的执行时，调用shutdown()方法来表示执行器应当结束，但是，执行器只有等待
 * 正在运行的任务或者等待执行的任务结束后，才能真正结束。
 * 如果在shutdown()方法与执行器结束之间发送一个任务给执行器，这个任务会被拒绝，
 * 因为这个时间段执行器已经不再接受任务了，ThreadPoolExecutor类提供了一套机制，
 * 当任务呗拒绝时调用这套机制来处理它们
 * 本节使用RejectedExecutionHandler接口，该接口只有一个方法
 * rejectedExecution方法
 *
 */
public class RejectedTaskController implements RejectedExecutionHandler{

	@Override  //在控制台输出已被拒绝的任务的名称和执行器的状态
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		System.out.printf("RejectedTaskController: The task %s has been rejected \n",r.toString());
		System.out.printf("RejectedTaskController: %s\n",executor.toString());
		System.out.printf("RejectedTaskController: Terminating: %s\n",executor.isTerminating());
		System.out.printf("RejectedTaskController: Terminated: %s\n",executor.isTerminated());
	}
}
