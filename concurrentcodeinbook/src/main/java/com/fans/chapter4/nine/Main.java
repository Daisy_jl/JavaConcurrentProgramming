package com.fans.chapter4.nine;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		ExecutorService executor = (ExecutorService)Executors.newCachedThreadPool();
		ResultTask resultTask [] = new ResultTask[5];
		for(int i = 0;i < 5;i++ ){
			//这里必须创建ExecutorTask 对象，然后创建ResultTask对象来使用ExecutorTask对象，最后调用
			//submit()方法将ResultTask发送给执行器
			ExecutableTask executableTask = new ExecutableTask("Task"+i);
			resultTask[i] = new ResultTask(executableTask); 
			
			executor.submit(resultTask[i]);
		}
		
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//取消任务的运行
		for(int i =0 ;i < resultTask.length;i++){
			resultTask[i].cancel(true);
		} 
		
		//通过调用ResultTask对象的get()方法，在控制台上输出还没有被取消的任务的结果
		for(int i =0;i < resultTask.length;i++){
			try {
				//这里会报异常：java.util.concurrent.CancellationException
				if(resultTask[i].isCancelled()){
					System.out.printf("%s\n",resultTask[i].get());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		executor.shutdown();//结束执行器
	}
}
