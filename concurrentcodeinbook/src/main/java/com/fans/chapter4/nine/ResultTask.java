package com.fans.chapter4.nine;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
/**
 * 
 * @author fcs
 * @date 2015-5-8
 * 描述：该类继承FutureTask并重写done()方法，该方法可以被用来执行一些后期操作
 * 说明：在创建好返回值以及改变任务状态为isDone()之后，FutureTask类就会在内部调用
 * done()方法。虽然我们无法改变任务的结果值，也无法改变任务的状态，但是可以通过任务来关闭系统
 * 资源，输出日志信息，发送通知等。
 */
public class ResultTask extends FutureTask<String>{
	private String name;
	public ResultTask(Callable<String> callable){
		super(callable);
		this.name =((ExecutableTask)callable).getName();
	}
	
	@Override
	protected void done() {
		if(isCancelled()){
			System.out.printf("%s: Has been canceld\n",name);
			
		}else{
			System.out.printf("%s: Has finished\n",name);
		}
	}
}
