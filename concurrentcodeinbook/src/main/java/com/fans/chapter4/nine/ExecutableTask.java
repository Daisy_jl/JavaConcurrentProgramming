package com.fans.chapter4.nine;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-5-8
 * 描述：在控制器中控制任务的完成
 * 说明：FutureTask类提供一个名为done()的方法，允许在执行器中的任务执行结束之后
 * 还可以执行一些代码，这个方法可以被用来执行一些后期处理操作。
 * 该方法默认的实现为空，我们可以继承该方法并重写该方法
 */
public class ExecutableTask implements Callable<String>{
	private String name;
	
	
	public ExecutableTask(String name) {
		this.name = name;
	}

	@Override
	public String call() throws Exception {
		long duration = (long)(Math.random()*10);
		System.out.printf("%s: waiting %d seconds for results.\n",this.name,duration);
		TimeUnit.SECONDS.sleep(duration);
		return "Hello, world. I am"+name;
	}
	
	public String getName(){
		return name;
	}
}
