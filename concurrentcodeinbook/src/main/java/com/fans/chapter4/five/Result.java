package com.fans.chapter4.five;
/**
 * 
 * @author fcs
 * @date 2015-5-5
 * 描述：运行多个任务并处理所有结果
 * 说明：
 */
public class Result {
	private String name;
	private int value;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
}
