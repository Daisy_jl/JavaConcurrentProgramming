package com.fans.chapter4.one;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Server {
	private ThreadPoolExecutor  executor;
	public Server(){
		//创建具有缓存功能的线程池
		executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
	}
	
	public void executeTask(Task task){
		System.out.println("Server: A new task has arrived!");
		executor.execute(task);   //调用执行器的execute()方法将任务发送给Task
		System.out.printf("Server: Pool Size : %d\n",executor.getPoolSize());
		System.out.printf("Server: Active Count: %d\n",executor.getActiveCount());
		System.out.printf("Server: Completed Tasks: %d\n",executor.getCompletedTaskCount());
	}
	
	public void endServer(){
		executor.shutdown();  
		//启动一次顺序关闭，执行以前提交的任务，但不接受新任务。如果已经关闭，则调用没有其他作用。
	}
	
}
