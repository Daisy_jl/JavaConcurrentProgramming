package com.fans.chapter4.four;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-5-5
 * 描述：运行多个任务并处理第一个结果
 * 说明：
 */
public class UserValidator {
	private  String name;

	public UserValidator(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean validate(String name,String password){
		Random random = new Random();
		//模拟处理时间
		long duration = (long)(Math.random() *10);
		System.out.printf("Validator %s: validating a user during %d seconds\n",this.name,duration);
		try {
			TimeUnit.SECONDS.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//返回随机的Boolean值，表示是否通过验证
		return random.nextBoolean();
	}
}
