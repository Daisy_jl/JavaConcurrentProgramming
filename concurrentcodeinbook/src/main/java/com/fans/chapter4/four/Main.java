package com.fans.chapter4.four;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
	public static void main(String[] args) {
		String user = "test";
		String password = "test";
		UserValidator  user1 = new UserValidator("LDAP");
		UserValidator  user2 = new UserValidator("DataBase");
		
		TaskValidator  task1 = new TaskValidator(user1, user, password);
		TaskValidator  task2 = new TaskValidator(user2, user, password);
		List<TaskValidator> task1List = new ArrayList<TaskValidator>();
		task1List.add(task1);
		task1List.add(task2);
		
		ExecutorService  executor = (ExecutorService)Executors.newCachedThreadPool();
		String result;
		
		try{
			result = executor.invokeAny(task1List);
			System.out.printf("Main : Result: %s\n",result);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		executor.shutdown();  //��ִֹ����
		System.out.println("Main : End of the execution");
	}
}
