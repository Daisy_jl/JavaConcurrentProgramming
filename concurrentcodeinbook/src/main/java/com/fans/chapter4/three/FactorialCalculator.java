package com.fans.chapter4.three;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author fcs
 * @date 2015-5-5
 * 描述：在执行器中执行任务并返回结果
 * 说明：多线程执行任务可以返回结果需要借助两个接口
 * 1.Callable接口，2.Future接口
 * 这里实现Callable接口
 */
public class FactorialCalculator implements Callable<Integer>{
	private Integer  number;
	
	public FactorialCalculator(Integer number) {
		super();
		this.number = number;
	}

	@Override
	public Integer call() throws Exception {
		int result = 1;
		if((number ==0)||(number ==1)){
			result = 1;
		}else{
			for(int i =2; i<= number;i ++){
				result *= result;
				TimeUnit.MILLISECONDS.sleep(20);
			}
		}
		System.out.printf("%s: %d\n",Thread.currentThread().getName(),result);
		return result;
	}
}
