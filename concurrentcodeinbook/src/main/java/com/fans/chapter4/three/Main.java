package com.fans.chapter4.three;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class Main {
   public static void main(String[] args) {
	  ThreadPoolExecutor  executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(2);
	  List<Future<Integer>> resultList = new ArrayList<Future<Integer>>();
      Random random = new Random();
	  for(int i =0;i< 10;i++){
    	  Integer  number = random.nextInt(10);
    	  FactorialCalculator fc = new FactorialCalculator(number);
    	  //这个方法返回一个Future<Integer>对象来管理任务和得到的最终结果
    	  Future<Integer>  result = executor.submit(fc);
    	  resultList.add(result);
	  }
	  //创建一个do循环来监控执行器的状态
	  do{
		  System.out.printf("Main: Number of Completed Tasks: %d\n",executor.getCompletedTaskCount());
		  for(int i =0;i < resultList.size();i++ ){
			  Future<Integer>  result = resultList.get(i);
			  //通过isDone()方法可以检查任务是否完成
			  System.out.printf("Main: Task %d: %s\n",i,result.isDone());
		  }
		  try{
			  TimeUnit.MILLISECONDS.sleep(50);
		  }catch(Exception e){
			  e.printStackTrace();
		  }
	  
	  }while(executor.getCompletedTaskCount() < resultList.size());
	  
	  System.out.println("Main result ");
	  
	  
	  for(int i =0 ;i< resultList.size();i++){
		  Future<Integer>result = resultList.get(i);
		  Integer number = null;
		  try {
			//对于每个Future来讲，通过get方法将得到由任务返回的Integer对象
			number = result.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		System.out.printf("Main: Task %d: %d\n",i,number);
	  }
	  
	  executor.shutdown();  //调用该方法结束执行。否则线程会继续执行下去的。
   }
}
