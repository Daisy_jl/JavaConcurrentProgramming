package com.fans.chapter4.six;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
/**
 * 
 * @author fcs
 * @date 2015-5-5
 * 描述：ScheduledThreadPoolExecutor:可以让任务在过一段时间后才被执行，或者任务能够被周期性的执行。
 */
public class Main {
	public static void main(String[] args) {
		
		ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor)Executors.newScheduledThreadPool(1);
		System.out.printf("Main: Starting at: %s\n",new Date());
		for(int i =0;i< 5;i++){
			Task task = new Task("Task"+i);
			//通过该方法启动这些任务
			executor.schedule(task, i+1, TimeUnit.SECONDS);
		}
		executor.shutdown();
		try {
			//通过该方法等待所有任务完成
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.printf("Main: Ends at: %s\n",new Date());
	}
}

