package com.fans.chapter4.six;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * 
 * @author fcs
 * @date 2015-5-5
 * 描述：在执行器中延迟执行任务
 * 说明：
 */
public class Task implements Callable<String> {

	private String name;
	
	public Task(String name) {
		this.name = name;
	}

	@Override
	public String call() throws Exception {
		System.out.printf("%s: Starting at: %s\n",name,new Date());
		return "return hello world";
	}
}
