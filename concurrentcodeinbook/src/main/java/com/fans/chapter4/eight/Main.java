package com.fans.chapter4.eight;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		ThreadPoolExecutor  executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();
		
		Task task = new Task();
		System.out.println("Main: Executing the task\n");
		//调用该方法将任务发送给执行器
		Future<String> result = executor.submit(task);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Main: Canceling the Task\n");
		//执行器的submit方法返回名为result的Future对象，调用Future对象的cancel方法来取消任务的执行。
		result.cancel(true);
		
		//这两个方法验证任务是否被取消和已完成
		System.out.printf("Main: Cancelled: %s\n",result.isCancelled());
		System.out.printf("Main: Done: %s\n",result.isDone());
	
		executor.shutdown();    //结束执行器
		
		System.out.println("Main: The executor has finished\n");
		
	}
}
