package com.fans.chapter4.eight;

import java.util.concurrent.Callable;
/**
 * 
 * @author fcs
 * @date 2015-5-8
 * 描述：在执行器中取消任务
 * 说明：有时候我们可能要取消已经发送给执行器的任务，。在这种情况下，可以使用Future的cancel()方法
 * 来执行取消操作
 */
public class Task implements Callable<String>{

	@Override
	public String call() throws Exception {
		while(true){
			System.out.printf("Task: Test\n");
			Thread.sleep(100);
		}
	}
}
